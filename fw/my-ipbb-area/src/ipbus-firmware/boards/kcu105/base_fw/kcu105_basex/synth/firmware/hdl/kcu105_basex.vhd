---------------------------------------------------------------------------------
--
--   Copyright 2017 - Rutherford Appleton Laboratory and University of Bristol
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
--                                     - - -
--
--   Additional information about ipbus-firmare and the list of ipbus-firmware
--   contacts are available at
--
--       https://ipbus.web.cern.ch/ipbus
--
---------------------------------------------------------------------------------


-- Top-level design for ipbus demo
--
-- This version is for KCU105 eval board, using SFP ethernet interface
--
-- You must edit this file to set the IP and MAC addresses
--
-- Dave Newbold, 24/10/16

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



use work.ipbus.ALL;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ipbus_example.all;
library UNISIM;
use UNISIM.VComponents.all;

entity top is port(
		sysclk_p: in std_logic;
		sysclk_n: in std_logic;
		eth_clk_p: in std_logic; -- 125MHz MGT clock
		eth_clk_n: in std_logic;
		eth_rx_p: in std_logic; -- Ethernet MGT input
		eth_rx_n: in std_logic;
		eth_tx_p: out std_logic; -- Ethernet MGT output
		eth_tx_n: out std_logic;
		leds: out std_logic_vector(7 downto 0); -- status LEDs
		dip_sw: in std_logic_vector(3 downto 0); -- switches
		push_sw : in std_logic_vector(1 downto 0); --pushbutton switches for reset
		
--		fmc_lpc_p : inout std_logic_vector(33 downto 0);
--		fmc_lpc_n : inout std_logic_vector(33 downto 0);


        FMC_ReSyncLoad : out std_logic;
        FMC_error_N      : in  std_logic;
        FMC_L1_ext     : out std_logic;
        FMC_sel_ck_ext : out std_logic;
        FMC_ReSync_ext : out std_logic;
        FMC_Strobe_ext : out std_logic;

-- TDC signals  
        FMC_trig1      : out std_logic;
        FMC_trig2      : out std_logic;
        FMC_probe_ToT  : in std_logic;
        FMC_probe_ToA_N  : in std_logic;

-- I2C signals  
        FMC_rstb       : out std_logic;
        FMC_I2C_rstb   : out std_logic;

        FMC_SDA        : inout std_logic;
        FMC_SCL        : out std_logic;

-- Fast commands  
        FMC_fcmd_n     : out std_logic;
        FMC_fcmd_p     : out std_logic;

-- DAC signals
        FMC_sync1n_DAC : out std_logic;
        FMC_sync2n_DAC : out std_logic;
        FMC_sclk_DAC   : out std_logic;
        FMC_din_DAC    : out std_logic;

-- PLL signals
        FMC_ck_40p     : out std_logic;
        FMC_ck_40n     : out std_logic;

        FMC_ck_320p    : out std_logic;
        FMC_ck_320n    : out std_logic;

        FMC_PLL_lock_N   : in std_logic; 


-- 1.28 Gbps links
        FMC_daq_n     : in std_logic_vector(1 downto 0);
        FMC_daq_p     : in std_logic_vector(1 downto 0);
        
        FMC_trigger_p : in std_logic_vector(3 downto 0);
        FMC_trigger_n : in std_logic_vector(3 downto 0);

		user_sma_gpio_p : out std_logic;
        user_sma_gpio_n  : in std_logic;
		
		FMC_VADJ_ON     : out std_logic;
		
		IIC_MUX_RESET_B_LS : out std_logic;
		IIC_MAIN_SCL_LS    : inout std_logic;
		IIC_MAIN_SDA_LS    : inout std_logic;

-- Debug, display the I2C errors
        I2C_error          : out std_logic_vector(4 downto 0);
        I2C_status         : out std_logic
	);

end top;

architecture rtl of top is

    signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);

    signal ctrl: ipb_reg_v(0 downto 0);	
    signal stat: ipb_reg_v(2 downto 0);

    signal clk_125,clk_40,clk_320,clk_640,clk_320_i,clk_640_i : std_logic;
    signal locked_40,locked_320,rst_320,rst_daq : std_logic;
	signal clk_ipb, rst_ipb, nuke, soft_rst, userled: std_logic;
	signal mac_addr: std_logic_vector(47 downto 0);
	signal ip_addr: std_logic_vector(31 downto 0);
	signal ipb_out: ipb_wbus;
	signal ipb_in: ipb_rbus;
	signal dbg : std_logic_vector(7 downto 0);
	
    signal i2c_main_ctrl,i2c_hgcroc_ctrl: ipb_reg_v(1 downto 0);
    signal i2c_main_stat,i2c_hgcroc_stat : ipb_reg_v(0 downto 0);
    
    signal scl_io ,sda_io : std_logic_vector(1 downto 0);
    signal FMC_SCL_int : std_logic;
	
begin



clkgen40 : entity work.clk_wiz_125_to_40
   port map ( 
   clk_out1 => clk_40,             
   reset => rst_ipb,
   locked => locked_40,
   clk_in1 => clk_125
 );

rst_320 <= not locked_40;

clkgen320 : entity work.clk_wiz_320
   port map ( 
   clk_320 => clk_320_i,             
   clk_640 => clk_640_i,
   reset => rst_320,
   locked => locked_320,
   clk_in1 => clk_40
 );
 rst_daq<=not locked_320;

--xilinx recommends using bufgce's to automatically manage skews
BUFGCE_DIV_320 : BUFGCE_DIV generic map (
BUFGCE_DIVIDE => 2,
IS_CE_INVERTED => '0',
IS_CLR_INVERTED => '0',
IS_I_INVERTED => '0'
)
port map ( 
  O  => clk_320,
  CE => '1',
  CLR => '0',
  I => clk_640_i
);

BUFGCE_DIV_640 : BUFGCE_DIV generic map (
BUFGCE_DIVIDE => 1,
IS_CE_INVERTED => '0',
IS_CLR_INVERTED => '0',
IS_I_INVERTED => '0'
)
port map ( 
  O  => clk_640,
  CE => '1',
  CLR => '0',
  I => clk_640_i
);




-- Infrastructure

	infra: entity work.kcu105_basex_infra
		port map(
			sysclk_p => sysclk_p,
			sysclk_n => sysclk_n,
			sysclk_o => clk_125,
			eth_clk_p => eth_clk_p,
			eth_clk_n => eth_clk_n,
			eth_tx_p => eth_tx_p,
			eth_tx_n => eth_tx_n,
			eth_rx_p => eth_rx_p,
			eth_rx_n => eth_rx_n,
			sfp_los => '0',
			clk_ipb_o => clk_ipb,
			rst_ipb_o => rst_ipb,
			nuke => nuke,
			soft_rst => soft_rst,
			leds => open,--leds(1 downto 0),
			mac_addr => mac_addr,
			ip_addr => ip_addr,
			ipb_in => ipb_in,
			ipb_out => ipb_out,
			dbg => dbg
		);
		
	leds(7 downto 0) <= dbg;
	
	nuke<=push_sw(0);
	soft_rst<=push_sw(1);	
	
		
	mac_addr <= X"020ddba1151" & dip_sw; -- Careful here, arbitrary addresses do not always work
	ip_addr <= X"c0a8c81" & dip_sw; -- 192.168.200.16+n

-- ipbus slaves live in the entity below, and can expose top-level ports
-- The ipbus fabric is instantiated within.



		
		
  fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_out,
      ipb_out => ipb_in,
      sel => ipbus_sel_ipbus_example(ipb_out.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
     );		
            


  slave0: entity work.ipbus_ctrlreg_v
	generic map (N_CTRL => 1, N_STAT => 3, N_INIT =>1)
	port map(
	  clk => clk_ipb,
	  reset => rst_ipb,
	  ipbus_in => ipbw(N_SLV_CTRL_REG),
	  ipbus_out => ipbr(N_SLV_CTRL_REG),
	  d => stat,
	  q => ctrl
	);
		
  stat(0) <= X"abcdfedd"; --board type, abcdfede for V1, abcdfedd for V2
  stat(1) <= X"00010009"; --firmware version 

--versions:
--1: SSTL1.2V
--2: LVDS (just swap the constraints file)
--3: SCL out (not inout)
--10004: enable FMC_VADJ_ON FMC_sel_ck_ext, disable FMC_I2C_rstb and IIC_MUX_RESET_B_LS after the firmware reset, add a FIFOs reset
--00005: added support for 640MHz output on clk320, enabled using bit 17 of hgcroc_daq.config
--10006: add 'start' and 'stop' signals for the Fast Commands, create a true 'sync' and 'dump' Fast commands.
--10007: add an external signal to trig the FSM of the DAQ controller.
--10008: add a 'capture' signal for each FIFO, add an 'automatic' trigger signal to start the DAQ FSM
--10009: add I2C error outputs on the FMC HPC

  
  FMC_I2C_rstb   <=ctrl(0)(0); 
  FMC_Sel_ck_ext <=ctrl(0)(1); 
  FMC_VADJ_ON    <=ctrl(0)(2); 
  IIC_MUX_RESET_B_LS <= ctrl(0)(3);
  
	
  stat(2)(0) <= FMC_error_N;
  stat(2)(1) <= FMC_pll_lock_N;
  stat(2)(2) <= FMC_probe_ToT;
  stat(2)(3) <= FMC_probe_ToA_N;

  hgroc_sc : entity work.hgroc_slow_control 
    port map(
      sc_clk => clk_ipb,  --clock for slow-control, does not need to be synchronous to ipbus
      ipbus_clk => clk_ipb,
      reset => rst_ipb,
                       
      ipbus_in => ipbw(N_SLV_HGROC_SC),
      ipbus_out => ipbr(N_SLV_HGROC_SC),
                       

    dac_mosi     => FMC_din_DAC,       --din_dac_FMC G24 OUT: data to DAC8411*
    dac_sclk     => FMC_sclk_DAC,       -- sclk_dac_FMC

    dac_sync2    => FMC_sync2n_DAC,       --G18 ...                   (outM)
    dac_sync1    => FMC_sync1n_DAC            
                                  
  );    
                     
                     

                     
                    
  hgroc_daq : entity work.hgroc_daq 
    port map(                
      ipbus_clk       => clk_ipb,
        reset           => rst_ipb,
        
        ipbus_in        => ipbw(N_SLV_HGROC_DAQ),
        ipbus_out       => ipbr(N_SLV_HGROC_DAQ),
        
        clk_320         => clk_320,
        clk_640         => clk_640,
        rst_daq         => rst_daq,                             
                                
        FMC_ck_40p      => FMC_ck_40p,
        FMC_ck_40n      => FMC_ck_40n,
        
        FMC_ck_320p     => FMC_ck_320p,
        FMC_ck_320n     => FMC_ck_320n,
        
        FMC_fcmd_P      => FMC_fcmd_P,
        FMC_fcmd_N      => FMC_fcmd_N,                            
        
        FMC_trigger_P   => FMC_trigger_P, 
        FMC_trigger_N   => FMC_trigger_N,
        
        FMC_data_P      => FMC_daq_P,
        FMC_data_N      => FMC_daq_N, 
    
        
        FMC_ReSyncLoad  => FMC_ReSyncLoad,
        FMC_ReSync_ext  => FMC_ReSync_ext ,
        
        FMC_L1_ext      => FMC_L1_ext,
        FMC_rstb        => FMC_rstb, 
        
        FMC_strobe_ext  => FMC_strobe_ext,
        FMC_trig1       => FMC_trig1,                 
        FMC_trig2       => FMC_trig2, 

        user_sma_gpio_p => user_sma_gpio_p,
        user_sma_gpio_n => user_sma_gpio_n,
        FMC_probe_ToT => FMC_probe_ToT,
        FMC_probe_ToA_N => FMC_probe_ToA_N

      );             
            
            

            

--I2C split into 2 interfaces instead of 2 busses of the same interface to make it more transparent if the HGCROC interface is moved into hgcroc_sc at some point            
  ipb_i2c_slave0: entity work.ipbus_ctrlreg_v
    generic map (N_CTRL => 2, N_STAT=>1) 
    port map(
      clk => clk_ipb,
      reset => rst_ipb,
      ipbus_in => ipbw(N_SLV_I2C_MAIN),
      ipbus_out => ipbr(N_SLV_I2C_MAIN),
      d => i2c_main_stat,
      q => i2c_main_ctrl
    );
           
  i2c_m0: entity work.i2c_master_top
    generic map (nbr_of_busses => 1) 
    port map (
      clk               => clk_ipb,
      reset             => rst_ipb,
      ------------------
      id_o              => open, 
      id_i              => x"00",
      enable            => i2c_main_ctrl(0)(15),
      bus_select        => i2c_main_ctrl(0)(12 downto 10),
      prescaler         => i2c_main_ctrl(0)( 9 downto  0),
      command           => i2c_main_ctrl(1),
      reply             => i2c_main_stat(0),
      ------------------
      scl_io(0)         => IIC_MAIN_SCL_LS,
      sda_io(0)         => IIC_MAIN_SDA_LS
      );           


  ipb_i2c_slave: entity work.ipbus_ctrlreg_v
    generic map (N_CTRL => 2, N_STAT=>1) 
    port map(
      clk => clk_ipb,
      reset => rst_ipb,
      ipbus_in => ipbw(N_SLV_I2C_HGCROC),
      ipbus_out => ipbr(N_SLV_I2C_HGCROC),
      d => i2c_hgcroc_stat,
      q => i2c_hgcroc_ctrl
    );
            
            
  i2c_m: entity work.i2c_master_top
    generic map (nbr_of_busses => 1) 
    port map (
      clk               => clk_ipb,
--      clk               => clk_40,
      reset             => rst_ipb,
      ------------------
      id_o              => open, 
      id_i              => x"00",
      enable            => i2c_hgcroc_ctrl(0)(15),
      bus_select        => i2c_hgcroc_ctrl(0)(12 downto 10),
      prescaler         => i2c_hgcroc_ctrl(0)( 9 downto  0),
      command           => i2c_hgcroc_ctrl(1),
      reply             => i2c_hgcroc_stat(0),
      ------------------
      scl_io(0)         => FMC_SCL_int,
      sda_io(0)         => FMC_SDA
      );           


FMC_SCL<=FMC_SCL_int;

I2C_error <= i2c_hgcroc_stat(0)(31 downto 27);
I2C_status <= i2c_hgcroc_stat(0)(26);

end rtl;
