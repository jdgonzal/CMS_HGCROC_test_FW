library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

library UNISIM;
use UNISIM.VComponents.all;

entity diff_out_320 is
    Generic ( is_diff : integer;
              swap:     integer :=0) ;
    Port ( clk_320 : in STD_LOGIC;
           clk_640 : in STD_LOGIC;
           rst : in STD_LOGIC;
           d : in STD_LOGIC;
           OutP : out STD_LOGIC;
           OutN : out STD_LOGIC;
           delay_in : in STD_LOGIC_VECTOR (10 downto 0);
           delay_wr : in STD_LOGIC;
           delay_out : out STD_LOGIC_VECTOR (10 downto 0);
           vtc : in STD_LOGIC);
end diff_out_320;

architecture Behavioral of diff_out_320 is

 signal d1,d2,d_i,d_j: std_logic;
 signal d4 : std_logic_vector(3 downto 0);
 signal delay_reg : std_logic_vector(1 downto 0) := "00";

begin

ODELAYE3_inst_c : ODELAYE3
    generic map (
      CASCADE => "NONE",
      DELAY_FORMAT => "COUNT",
      DELAY_TYPE => "VAR_LOAD",
      DELAY_VALUE => 0,
      IS_CLK_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REFCLK_FREQUENCY => 320.0,
      UPDATE_MODE => "ASYNC"
    )
    port map (
      CASC_OUT => open,
      CNTVALUEOUT => delay_out(8 downto 0),
      DATAOUT => d2,
      CASC_IN => '0',
      CASC_RETURN => '0',
      CE => '0',
      CLK => clk_320,
      CNTVALUEIN => delay_in(8 downto 0),
      EN_VTC => vtc,
      INC => '0',
      LOAD => delay_wr,  
      ODATAIN => d1,
      RST => rst
    );
    
    



o_clk40_serdes : OSERDESE3 generic map (
   DATA_WIDTH => 4,
   INIT => '0',
   IS_CLKDIV_INVERTED => '0',
   IS_CLK_INVERTED => '0',
   IS_RST_INVERTED => '0'
)
port map (
   OQ      => d1,
   T_OUT   => open,
   CLK     => clk_640,
   CLKDIV  => clk_320,
   D(7 downto 4) => "0000",
   D(3 downto 0)       => d4,
   RST     => rst,
   T       => '0'
);

o_inst_diff: if is_diff=1  generate
  
  o_inst_diff_nswap : if swap=0 generate  
    o_clk40_obuf  : obufds port map ( i=>d2, o=>OutP, ob=>OutN );
  end generate;  
  
  o_inst_diff_swap : if swap=1 generate  
      o_clk40_obuf  : obufds port map ( i=>d2, o=>OutN, ob=>OutP );
    end generate;
end generate;

o_inst_se: if is_diff=0  generate
  OutP<=d2;
  OutN<='0';
end generate;


delay_out(10 downto 9)<=delay_reg;

process (clk_320, rst)
 variable i : std_logic_vector(1 downto 0);
 variable d4a : std_logic_vector(3 downto 0); 
begin
  if rising_edge(clk_320) then
    d_i<=d;
    
    if delay_wr='1' then
      delay_reg<=delay_in(10 downto 9);
    end if;
    
    i:=d&d_i;
    
    case i is
      when "00" => 
        d4a:="0000";

      when "11" => 
        d4a:="1111";

      when "10" =>          
        case delay_reg(1 downto 0)  is
          when "00" => 
            d4a:="1111";
          when "01" => 
            d4a:="1110";
          when "10" => 
            d4a:="1100";
          when "11" => 
            d4a:="1000";
        end case;
   
      when "01" => 
        case delay_Reg(1 downto 0) is
          when "00" => 
            d4a:="0000";
          when "01" => 
            d4a:="0001";
          when "10" => 
            d4a:="0011";
          when "11" => 
            d4a:="0111";
        end case;
 
   end case;
   
   if swap=1 then
     d4<=not d4a;
   else
     d4<=d4a;
   end if;
  
  end if;
end process;


end Behavioral;
