----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.05.2019 16:10:36
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity hgroc_fci is
    Port (                       
           ipbus_clk : in STD_LOGIC;           
           ipbus_rst : in STD_LOGIC;
           ipbus_in  : in ipb_wbus;
           ipbus_out : out ipb_rbus;
           clk       : in std_logic; --clock for the read domain, the remaining signals should be synchronous to this               
           rst       : in std_logic;
           trig      : in std_logic;
           busy      : out std_logic;
           d_out     : out std_logic_vector(7 downto 0)      
           );
end entity;           

architecture Behavioral of hgroc_fci is


signal addr_i : std_logic_vector(9 downto 0);
signal data_i : std_logic_vector(31 downto 0);
signal we, en : std_logic;
signal addr_o, c : std_logic_vector(12 downto 0);

signal ipbus_ack : std_logic;



begin

ram_inst: entity work.blk_mem_fci
                port map(
                  clka => ipbus_clk,
                  addra => addr_i,
                  wea(0) => we,
                  dina => data_i,

                  clkb => clk,
                  addrb => addr_o,
                  doutb => d_out
                  );
                  
                  

addr_i<=ipbus_in.ipb_addr(12 downto 0);
data_i<=ipbus_in.ipb_wdata;
we <='1' when ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1';





-- otest state machine
process(rst,clk)
begin
  if rst='1' then
    c<=(others => '0');
    addr_o<=(others => '0');
  elsif rising_edge(clk) then
    addr_o<=c;    
    if en='1' then
      c<=std_logic_vector(unsigned(c)+1);
    else   
      c<=(others=> '0');
    end if;
    
    if trig='1' then 
      en<='1';
    end if;    
  end if;
end process;

busy <= en;

process(ipbus_rst, ipbus_clk)
begin
  if ipbus_rst='1' then   
    ipbus_ack<='0';    
  elsif rising_edge(ipbus_clk) then
    ipbus_ack <='0';    
    if ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1' then
      ipbus_ack<='1';
    end if;
  end if;
end process;    
    


ipbus_out.ipb_err <= '0';
ipbus_out.ipb_ack <= ipbus_ack;  

end Behavioral;
