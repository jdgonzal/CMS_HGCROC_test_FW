##================================================================================##
##======  KCU105 FMC LPC J2 PIN OUT TO HGCROCv2 FLIPCHIP TESTBOARD CARRIER  ======##
##================================================================================##

# FMC pin D17
set_property PACKAGE_PIN AA20 [get_ports FMC_ReSyncLoad]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_ReSyncLoad]
# FMC pin D21
set_property PACKAGE_PIN AB32 [get_ports FMC_error]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_error]
# FMC pin D23
set_property PACKAGE_PIN AD30 [get_ports FMC_L1_ext]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_L1_ext]
# FMC pin D24
set_property PACKAGE_PIN AD31 [get_ports FMC_sel_ck_ext]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_sel_ck_ext]
# FMC pin D26
set_property PACKAGE_PIN AF33 [get_ports FMC_ReSync_ext]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_ReSync_ext]
# FMC pin D27
set_property PACKAGE_PIN AG34 [get_ports FMC_Strobe_ext]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_Strobe_ext]

##==============##
## TDC signals  ##
##==============##

# FMC pin C18
set_property PACKAGE_PIN U21 [get_ports FMC_trig1]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_trig1]
# FMC pin C22
set_property PACKAGE_PIN AB30 [get_ports FMC_trig2]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_trig2]
# FMC pin D12
set_property PACKAGE_PIN V28 [get_ports FMC_probe_ToT]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_probe_ToT]
# FMC pin D14
set_property PACKAGE_PIN V26 [get_ports FMC_probe_ToA]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_probe_ToA]

##==============##
## I2C signals  ##
##==============##

# FMC pin C26
set_property PACKAGE_PIN AG31 [get_ports FMC_rstb]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_rstb]

# FMC pin D8 
set_property PACKAGE_PIN W25 [get_ports FMC_I2C_rstb]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_I2C_rstb]

# FMC pin D9 
set_property PACKAGE_PIN Y25 [get_ports FMC_SDA]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_SDA]

# FMC pin D11
set_property PACKAGE_PIN V27 [get_ports FMC_SCL]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_SCL]

##================##
## Fast commands  ##
##================##

# FMC pin G30
set_property PACKAGE_PIN U34 [get_ports FMC_fcmd_n]
# FMC pin G31
set_property PACKAGE_PIN V34 [get_ports FMC_fcmd_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports FMC_fcmd_p]

##=================##
## DAC signals ##
##=================##

# FMC pin C10
set_property PACKAGE_PIN V29 [get_ports FMC_sync1n_DAC]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_sync1n_DAC]

# FMC pin C11
set_property PACKAGE_PIN W29 [get_ports FMC_sync2n_DAC]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_sync2n_DAC]

# FMC pin C14
set_property PACKAGE_PIN T22 [get_ports FMC_sclk_DAC]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_sclk_DAC]

# FMC pin C15
set_property PACKAGE_PIN T23 [get_ports FMC_din_DAC]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_din_DAC]

##=================##
## PLL signals ##
##=================##

# FMC pin G6 
set_property PACKAGE_PIN W23 [get_ports FMC_ck_40p]
# FMC pin G7 
set_property PACKAGE_PIN W24 [get_ports FMC_ck_40n]
set_property IOSTANDARD DIFF_SSTL12 [get_ports FMC_ck_40p]

# FMC pin H7 
set_property PACKAGE_PIN AA22 [get_ports FMC_ck_320p]
# FMC pin H8 
set_property PACKAGE_PIN AB22 [get_ports FMC_ck_320n]
set_property IOSTANDARD DIFF_SSTL12 [get_ports FMC_ck_320p]

# FMC pin D20
set_property PACKAGE_PIN AA32 [get_ports FMC_PLL_lock]
set_property IOSTANDARD LVCMOS12 [get_ports FMC_PLL_lock]

##=================##
## 1.28 Gbps links ##
##=================##

# FMC pin G33
set_property PACKAGE_PIN V33 [get_ports FMC_daq_n[1]]
# FMC pin G34
set_property PACKAGE_PIN W34 [get_ports FMC_daq_p[1]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_daq_p[1]]

# FMC pin G36
set_property PACKAGE_PIN W33 [get_ports FMC_daq_n[0]]
# FMC pin G37
set_property PACKAGE_PIN Y33 [get_ports FMC_daq_p[0]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_daq_p[0]]

# FMC pin H28
set_property PACKAGE_PIN AE32 [get_ports FMC_trigger_p[2]]
# FMC pin H29
set_property PACKAGE_PIN AF32 [get_ports FMC_trigger_n[2]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_trigger_p[2]]

# FMC pin H31
set_property PACKAGE_PIN V31 [get_ports FMC_trigger_p[3]]
# FMC pin H32
set_property PACKAGE_PIN W31 [get_ports FMC_trigger_n[3]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_trigger_p[3]]

# FMC pin H34
set_property PACKAGE_PIN Y31 [get_ports FMC_trigger_p[1]]
# FMC pin H35
set_property PACKAGE_PIN Y32 [get_ports FMC_trigger_n[1]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_trigger_p[1]]

# FMC pin H37
set_property PACKAGE_PIN W30 [get_ports FMC_trigger_p[0]]
# FMC pin H38
set_property PACKAGE_PIN Y30 [get_ports FMC_trigger_n[0]]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports FMC_trigger_p[0]]

##==========##
## MGT(GTX) ##
##==========##
