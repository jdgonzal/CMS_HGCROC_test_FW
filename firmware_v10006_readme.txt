2019/09/06

Firmware Version is 0x00010006

Firmware upgrade.
add  'start' and 'stop' signals for the Fast Commands, create a true 'sync' and 'dump' Fast commands.

*******************************************************************
#Fast command triggers, must be on a an integer multiple of 8 to have any effect.
./fmc_hgroc  -W hgroc_daq.event.fcmd_orbit_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_orbit_stop=200

./fmc_hgroc  -W hgroc_daq.event.fcmd_calib_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_calib_stop=200

./fmc_hgroc  -W hgroc_daq.event.fcmd_trig_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_trig_stop=232

./fmc_hgroc  -W hgroc_daq.event.fcmd_sync_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_sync_stop=200

./fmc_hgroc  -W hgroc_daq.event.fcmd_dump_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_dump_stop=200

******************************************************************
