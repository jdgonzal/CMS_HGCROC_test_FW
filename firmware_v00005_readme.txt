2019/08/05

Firmware Version is 0x00000005

Add support for 640MHz clock on clk320. 

*******************************************************************

To switch from 320 to 640MHz set bit 17 of hgroc_daq.config to 1:
./fmc_hgroc -W hgroc_daq.config=0x20000


******************************************************************
