#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 10, suffix = ""):

    ev_size = 44

    n_bxs = 5 # number of consecutive L1A (max for same Orbit is 12)
    data_size = (ev_size - 1) * n_events * n_bxs

    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
        data_size = (ev_size - 1) * n_events

    ## Set DAC values
    calib_dac = 50
    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    #set_roc_parameter(df_params, "ReferenceVoltage", "all", "ExtCtest", 0)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", calib_dac)

    ## Output folder
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/sampling_scan/run_" + timestamp + "_calDAC_%i"%calib_dac + suffix

    print "Output dir:"
    print odir

    ## disable all channels:
    #print("Disabling all channels")
    #set_roc_parameter(df_params, "ch", "all", "LowRange", 0)

    ## Fast commands
    disable_fcmds()

    strobe = 0
    calib = strobe + 1
    calib_stop = calib + 1
    orbit = calib + calib_offset

    set_fcmd_orbit(orbit, orbit + 1)
    set_fcmd_calib(calib, calib_stop)
    set_gpio_p(calib, calib_stop)

    # data readout
    l1a_start = calib + calib_offset + l1a_offset
    l1a_stop = l1a_start + 1

    if n_bxs > 1:
        l1a_stop += capture_offset
        l1a_stop += (ev_size - 1) * (n_bxs - 1)

    set_fcmd_l1a(l1a_start, l1a_stop)

    c_start = l1a_start + capture_offset
    c_stop = c_start + (ev_size - 1) * n_bxs
    set_capture_window(c_start, c_stop)

    print l1a_start, c_start, c_stop

    ## trigger readout
    tp_link_offset = capture_offset + 5
    tp_ev_size = 1 * n_bxs
    tp_data_size = n_events * tp_ev_size

    tp_c_start = calib + tp_link_offset
    tp_c_stop = tp_c_start + tp_ev_size

    for i in range(4):
        set_fifo_capture_window(i, tp_c_start, tp_c_stop)
    #print tp_c_start, tp_c_stop

    ## Channel loop
    for chan in range(0,36,9):
        chans = [chan, chan+36]
        print("Enabling channels:")
        print(chans)

        for ch in chans:
            set_roc_parameter(df_params, "ch", ch, "LowRange", 1)
            #set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

        for phase in range(16):
            print "Setting clock phase to %i" %phase
            set_roc_parameter(df_params, "Top", "all","Phase", phase)

            outdir = odir + "/chan_%i/phase_%i" %(chan,phase)
            outdir = os.path.abspath(outdir) + "/"

            if not os.path.exists(outdir):
                os.makedirs(outdir)

            flush_fifos()
            bash_acq_loop(n_events, 0)

            # read data
            read_fifos([4,5],outdir + "/", data_size)
            # read triger
            read_fifos(range(4),outdir + "/", tp_data_size)

        ## end phase loop

        # disable channels
        for ch in chans:
            #disable_ch_inj(ch)
            set_roc_parameter(df_params, "ch", ch, "LowRange", 0)
            #set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

    ## end chan loop
    return

def main():

    n_events = 1
    suf = ""
    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)

    if len(sys.argv) > 2:
        suf = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suf)

    acq(n_events, suf)

if __name__ == "__main__":

    main()
