#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 5):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/toa_scan/run_" + timestamp

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    # sampling phase
    phase = 14
    bx_offset = -1

    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    print "Setting clock phase to %i" %phase , " with word" , ret

    strobe = 0
    calib = strobe
    l1a = strobe + 10 + l1a_offset + bx_offset

    c_start = l1a + capture_offset
    ev_size = 40
    c_stop = c_start + ev_size

    # Fast CMDs
    set_fcmds(orb = 1e4, l1a = l1a, calib = calib)
    set_capture_window(c_start, c_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    #toa_thrs = range(0,255,10)
    #toa_thrs = range(0,100,10)
    toa_thrs = range(25,115,10)
    print("TOA thresholds:")
    print(toa_thrs)

    calDAC_toTOAthr_slope = 13
    calDAC_toTOAthr_offset = -300

    print("Disabling all channels")
    ## disable all channels:
    for chan in range(72):
        disable_ch_inj(chan)

    tot_thr = 255
    print "Setting TOT thr:", tot_thr
    set_tot_threshold(tot_thr)

    print("Starting scan")
    #for chan in range(0,36,1):
    for chan in range(0,36,4):

        print 80*"#"
        chans = [chan, chan+36]
        print "Channels:", chans

        # enable chans
        for ch in chans:
            enable_ch_inj(ch, range_capa = "low")

        for toa_thr in toa_thrs:
            print "TOA thr:", toa_thr
            set_toa_threshold(toa_thr)

            '''
            min_caldac = int((toa_thr * calDAC_toTOAthr_slope + calDAC_toTOAthr_offset) * 0.8)
            min_caldac = max(min_caldac, 0)

            max_caldac = int((toa_thr * calDAC_toTOAthr_slope + calDAC_toTOAthr_offset) * 1.2)
            max_caldac = min(max_caldac, 2047)

            #max_caldac = int(1.1 * toa_thr * calDAC_toTOAthr_slope)
            step_caldac = 10
            print("Cal DAC range: from %i to %i with step %i" %(min_caldac, max_caldac, step_caldac))
            '''
            min_caldac = 0
            max_caldac = 1300
            step_caldac = 10

            calib_dacs = range(min_caldac, max_caldac, step_caldac)
            #calib_dacs = range(0,2000,2)

            for calib_dac in calib_dacs:
                print calib_dac
                #print "Setting calib_dac", calib_dac
                set_roc_parameter(df_params, "ReferenceVoltage", "all","Calib_dac", phase)

                outdir = odir + "/chan_%i/TOAthr_%i/calibDAC_%i" %(chan, toa_thr, calib_dac)
                outdir = os.path.abspath(outdir) + "/"

                if not os.path.exists(outdir):
                    os.makedirs(outdir)

                flush_fifos()
                bash_acq_loop(n_events, 0)

                if check_fifo_full():
                    for ififo in range(4,6):
                        try:
                            read_fifo(ififo,outdir + "/", ev_size * n_events)
                            continue
                        except ValueError:
                            print("Error")

            print
            ## end calib loop
        ## end thr loop

        # disable channels
        for ch in chans:
            disable_ch_inj(ch)

    ## end chan loop
    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
