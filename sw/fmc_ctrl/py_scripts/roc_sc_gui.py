# --- for GUI -------
from Tkinter import *
import Tkinter as ttk
from ttk import *
# -------------------

from random import randint
#from i2c_params import *

from hgcroc_i2c_wrapper import *

if __name__ == "__main__":

    if len(sys.argv) == 2:
        print sys.argv
        bitfname = sys.argv[1]
    else:
        bitfname = "configs/HGCROCv2_I2C_Params.csv"

    df_params = load_param_df(bitfname)

    block_list = get_block_list(df_params)
    block_counts = get_blockid_counts(df_params)

    ## GUI management
    root = Tk()
    root.title("HGCROCv2 configuration menu")

    # Add a grid
    mainframe = Frame(root)
    mainframe.grid(column=0,row=0, sticky=(N,W,E,S) )
    mainframe.columnconfigure(0, weight = 1)
    mainframe.rowconfigure(0, weight = 1)
    mainframe.pack(pady = 100, padx = 100)

    ## Field 1
    # Create a Tkinter variable
    gname_var = StringVar(root)

    # Dictionary with options
    choices = ["--"] + block_list
    gname_var.set('--') # set the default option

    popupMenu = OptionMenu(mainframe, gname_var, *choices)
    Label(mainframe, text="SubBlock").grid(row = 1, column = 1)
    popupMenu.grid(row = 1, column =2)

    ## Field 2
    # Create a Tkinter variable
    idx_var = StringVar(root)

    # Dictionary with options
    choices2 = ["--"]
    idx_var.set('--')

    Label(mainframe, text="Block ID").grid(row = 2, column = 1)
    popupMenu2 = OptionMenu(mainframe, idx_var, *choices2)
    popupMenu2.grid(row = 2, column =2)

    ## Field 3
    # Create a Tkinter variable
    param_var = StringVar(root)

    # Dictionary with options
    choices3 = ["--"]
    param_var.set('--')

    popupMenu3 = OptionMenu(mainframe, param_var, *choices3)
    Label(mainframe, text="Parameter").grid(row = 3, column = 1)
    popupMenu3.grid(row = 3, column =2)

    ## spacer label
    Label(mainframe, text="").grid(row = 4, column = 1)

    Label(mainframe, text="Value").grid(row = 5, column = 1)

    var = StringVar()
    textbox = Entry(mainframe, textvariable=var, width = 10)
    #textbox.focus_set()
    #textbox.pack(pady=5, padx=5)
    textbox.grid(row = 5, column =2)#, columnspan = 5)

    nbits = StringVar()
    nbits.set("-- bits")
    textbox = Label(mainframe, textvariable=nbits)
    textbox.grid(row = 5, column =3)


    def callback_db():
        print( "Pressed default button")

        print gname_var.get(), idx_var.get(), param_var.get()

        val = get_param_default( df_params, gname_var.get(), int(idx_var.get()), param_var.get())
        var.set(val)

    def callback_wb():
        print( "Pressed wb")
        #n_bits = get_param_nbits(df_params, gname_var.get(), param_var.get() )
        #val = "Nbits: %i" % n_bits
        #var.set(val)
        val = int(var.get())

        print set_roc_parameter( df_params, gname_var.get(), int(idx_var.get()), param_var.get(), val )

    def callback_rb():
        print( "Pressed rb")
        print gname_var.get(), idx_var.get(), param_var.get()

        val = read_roc_parameter( df_params, gname_var.get(), int(idx_var.get()), param_var.get())
        var.set(val)

    ## spacer label
    Label(mainframe, text="").grid(row = 6, column = 1)

    ## Buttons
    rb = Button(mainframe, text = "Read",command=callback_rb)
    rb.grid(row = 7, column = 1)
    wb = Button(mainframe, text = "Write",command=callback_wb)
    wb.grid(row = 7, column = 2)
    db = Button(mainframe, text = "Set default", command = callback_db)
    db.grid(row = 7, column = 3)

    # on change dropdown value
    def change_dropdown(*args):
        print( gname_var.get() )

        var = gname_var.get()
        #if var in idx_list:
        if var in block_list:
            #print ("In list!")
            idxs = get_blockid_list(df_params, var)

            idxs = [str(val) for val in idxs]
            #idxs = [str(val) for val in range(idx_list[var])]
            popupMenu2.set_menu(*idxs)

            if var == "cm" or var == "calib": var = "ch"
            #params = param_list[var]
            params = get_param_list(df_params, var)

            popupMenu3.set_menu(*params)
        else:
            print ("Not in list!")

        print gname_var.get(), idx_var.get(), param_var.get()

        n_bits = get_param_nbits(df_params, gname_var.get(), param_var.get() )
        val = "Nbits: %i" % n_bits #nbits_params[( gname_var.get(), param_var.get())]
        nbits.set(val)

    def change_dropdown3(*args):
        n_bits = get_param_nbits(df_params, gname_var.get(), param_var.get() )
        val = "Nbits: %i" % n_bits
        #val = "Nbits: %i" % nbits_params[( gname_var.get(), param_var.get())]
        nbits.set(val)

    # link function to change dropdown
    gname_var.trace('w', change_dropdown)
    # link function to change dropdown
    #idx_var.trace('w', change_dropdown)
    # link function to change dropdown
    param_var.trace('w', change_dropdown3)


    root.mainloop()
