#!/usr/bin/python
import sys, os, datetime
import numpy as np
from common import *

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

n_events = 1
n_patterns = 256

def get_best_delay(delays, nidles):

    sel = nidles >= n_patterns - 1
    print "%i points at n_patterns nidles" % np.count_nonzero(sel)

    best_del = -1
    rms = 0

    if np.count_nonzero(sel) > 0:
        best_del = int(delays[sel].mean())
        rms = int(delays[sel].std())

        if rms > 50:
            print("!!! Too wide RMS: %i. Narrowing down window" %rms)
            #sel &= (delays - best_del) < rms
            sel &= (delays - delays[sel].min() ) < 100
            best_del = int(delays[sel].mean())

            ## print std
            rms = int(delays[sel].std())
            if rms > 50:
                print("!!! Too wide RMS: %i" %rms)
                best_del = -1

    print "best del/RMS:", best_del, rms

    return best_del


def plot_best_delays(all_vals, outdir = "./", min_idles = n_patterns):
    best_delays = {}

    f,axs = plt.subplots(2,3, figsize = (12,8), sharey=True)
    #f,axs = plt.subplots(1,1, figsize = (12,8), sharey=True)

    for ififo in range(6):
        ax = f.get_axes()[ififo]

        vals = all_vals[ififo]
        delays = np.array(vals.keys())
        nidles =  np.array(vals.values())

        # sort by delays
        idxs = delays.argsort()
        delays = delays[idxs]
        nidles = nidles[idxs]

        print 80*"-"
        print "Fifo", ififo,

        best_del = get_best_delay(delays, nidles)

        if best_del > -1:

            print "Best delay:", best_del, hex(best_del)
            best_delays[ififo] = best_del

            ax.plot(delays,nidles, "o-", color = "C%i" %ififo,
                    label = "Fifo %i, delay:\n%i " %(ififo,best_del) + hex(best_del))

            ax.axvline(best_del, color = "k", linestyle = "--")#"C%i" %ififo)

        else:
            print("Best delay NOT FOUND")

        ax.axhline(n_patterns, color = "k", linestyle = "--")

    print 80*"-"
    best_mean = int(np.mean((best_delays.values())))
    print "Mean best delay", best_mean, hex(best_mean)

    for ax in f.get_axes():
        ax.legend(loc = 3)#, ncol = 2)
        ax.grid()

        ax.set_xlabel("delay")
        ax.set_ylabel("N of idles")

    #axs[0][0].set_title(outdir.replace("/"," "))
    title = os.path.dirname(outdir)
    title = os.path.basename(title)
    plt.suptitle(title)

    #plt.show() #uncomment to show plot

    print
    for ext in [".png", ".pdf"]:
        figname = outdir + "/" + "delay_scan" + ext
        plt.savefig(figname)
        print("Saved figure as " + figname)

    ## print best delays in fmc_hgroc format:
    print
    print "Best delays:"
    print",".join([str(int(v)) for v in best_delays.values()])
    print",".join([str(hex(v)) for v in best_delays.values()])

    '''
    print("Output for fmc_hgroc config:")
    for ififo in range(6):
        if ififo in best_delays:
            print("./fmc_hgroc  -W hgroc_daq.delay.fifo%i="%ififo + hex(best_delays[ififo]))
    '''

    return best_delays

def scan_delays(del_step = 1, min_del = 0, max_del = 512):

    #print("Going to scan through %i points from %i to %i" % ((max_del - min_del)/del_step, min_del, max_del)

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/delays/run_" + timestamp
    odir = os.path.abspath(odir) + "/"

    if not os.path.exists(odir):
        os.makedirs(odir)

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    strobe = 0

    # Fast CMDs
    #set_fcmds(orb = strobe, l1a = 1e4, calib = strobe)
    set_fcmd_sync(strobe, strobe + 1)
    set_fcmd_orbit()
    set_fcmd_l1a()
    set_fcmd_calib()
    set_fcmd_dump()

    data_size = n_patterns

    link_latency = 9 # needed to allow for the synch pattern capture to be recognized

    c_start = strobe + link_latency
    c_stop = c_start + data_size

    set_capture_window(c_start, c_stop)
    set_gpio_p(c_start, c_stop)

    # take into account n of events for reading
    data_size *= n_events

    # check fifo is empty
    print "Fifo full?", check_fifo_full()
    print ("Taking data...")

    all_vals = {i:{} for i in range(6)}

    print "Delay",
    for ififo in range(6): print "\tFifo", ififo,
    print

    for delay in range(min_del, max_del, del_step):
        print delay,

        flush_fifos()

        outdir = odir + "/delay_%i_" %delay

        #delays = [delay for i in range(6)]
        delays = {i:delay for i in range(6)}
        set_link_delays(delays, vtc_on = 0)

        #trig_fsm(trig = 1)
        bash_acq_loop(n_events, 1)

        #print "FIFO full?", check_fifo_full()
        #print "#", check_fifo_content()

        for ififo in range(6):
            ret = read_fifo(ififo,outdir, data_size)
            fname = outdir + "fifo%i.raw" % ififo

            nidles = 0
            with open(fname) as f:
                lines = f.read().splitlines()
                nidles = lines.count(idle_word)

            all_vals[ififo][delay] = int(nidles / float(n_events))

            #print "\t", ififo,":", nidles / float(n_events),
            print "\t", int(nidles / float(n_events)),
        print

    print ("... done")

    ### Compute best delays
    print 80*"#"
    best_delays = plot_best_delays(all_vals, outdir = odir)

    ## set delays
    print("Setting best delays")
    set_link_delays(best_delays, vtc_on = 1)

    ## update link delay settings
    daq.link_delays = best_delays
    daq.save_config()#"configs/test.yaml")

    return

def main():

    min_del = 0
    max_del = 512
    del_step = 32

    if len(sys.argv) > 1:
        del_step = int(sys.argv[1])
        print("## %i delay step requested" %del_step)

    if len(sys.argv) > 2:
        min_del = int(sys.argv[2])
        print("## %i min delay" %min_del)

    if len(sys.argv) > 3:
        max_del = int(sys.argv[3])
        print("## %i max delay" %max_del)

    scan_delays(del_step, min_del, max_del)

if __name__ == "__main__":

    main()
