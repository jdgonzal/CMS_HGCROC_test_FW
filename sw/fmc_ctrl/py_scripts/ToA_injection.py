#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

##### Probe PLL
# set_roc_parameter(df_params, "Top", 0, "En_pll_ext", probe)

##### Bypass PLL
# set_ext_clock(ext) #IPbus register to set FMC_Sel_ck_ext signal
# set_roc_parameter(df_params, "Top", 0, "Sel_40M_ext", ext) #ROC register in Top sub-block

##### Master TDC sub block parameters list
# # Register 0
# GLOBAL_TA_SELECT_GAIN_TOA
# GLOBAL_TA_SELECT_GAIN_TOT

# # Register 1
# GLOBAL_MODE_NO_TOT_SUB
# GLOBAL_LATENCY_TIME
# GLOBAL_MODE_FTDC_TOA_S0
# GLOBAL_MODE_FTDC_TOA_S1
# GLOBAL_SEU_TIME_OUT

# # Register 2
# BIAS_FOLLOWER_CAL_P_D
# BIAS_FOLLOWER_CAL_P_EN
# INV_FRONT_40MHZ
# START_COUNTER
# CALIB_CHANNEL_DLL

# # Register 3
# VD_CTDC_P_D
# VD_CTDC_P_DAC_EN
# EN_MASTER_CTDC_VOUT_INIT
# EN_MASTER_CTDC_DLL

# # Register 4
# BIAS_CAL_DAC_CTDC_P_D # MSB in register 7
# CTDC_CALIB_FREQUENCY

# # Register 5
# GLOBAL_MODE_TOA_DIRECT_OUTPUT
# BIAS_I_CTDC_D
# FOLLOWER_CTDC_EN

# # Register 6
# GLOBAL_EN_BUFFER_CTDC
# VD_CTDC_N_FORCE_MAX
# VD_CTDC_N_D
# VD_CTDC_N_DAC_EN

# # Register 7
# CTRL_IN_REF_CTDC_P_D
# CTRL_IN_REF_CTDC_P_EN
# BIAS_CAL_DAC_CTDC_P_D # LSB in register 4

# # Register 8
# CTRL_IN_SIG_CTDC_P_D
# CTRL_IN_SIG_CTDC_P_EN
# GLOBAL_INIT_DAC_B_CTDC
# BIAS_CAL_DAC_CTDC_P_EN

# # Register 9
# VD_FTDC_P_D
# VD_FTDC_P_DAC_EN
# EN_MASTER_FTDC_VOUT_INIT
# EN_MASTER_FTDC_DLL

# # Register 10
# BIAS_CAL_DAC_FTDC_P_D # MSB in register 14
# FTDC_CALIB_FREQUENCY

# # Register 11
# EN_REF_BG
# BIAS_I_FTDC_D
# FOLLOWER_FTDC_EN

# # Register 12
# GLOBAL_EN_BUFFER_FTDC
# VD_FTDC_N_FORCE_MAX
# VD_FTDC_N_D
# VD_FTDC_N_DAC_EN

# # Register 13
# CTRL_IN_SIG_FTDC_P_D
# CTRL_IN_SIG_FTDC_P_EN
# GLOBAL_INIT_DAC_B_FTDC
# BIAS_CAL_DAC_FTDC_P_EN

# # Register 14
# CTRL_IN_REF_FTDC_P_D
# CTRL_IN_REF_FTDC_P_EN
# BIAS_CAL_DAC_FTDC_P_D # LSB in register 10

# # Register 15
# GLOBAL_DISABLE_TOT_LIMIT
# GLOBAL_FORCE_EN_CLK
# GLOBAL_FORCE_EN_OUTPUT_DATA
# GLOBAL_FORCE_EN_TOT

def acq(n_events = 1):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    #odir = "./data/injection/run_" + timestamp
    odir = "./data/injection/"#run_" + timestamp

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    # config inj

    ## disable all channels:
    if False:
        set_toa_threshold(0xff)
        set_tot_threshold(0xff)

        print("Disabling all channels")
        for chan in range(72):
            #disable_ch_inj(chan)
            mask_toa(chan, 1)
            mask_tot(chan, 1)

    ext_trig_number = 0 #Select TDC external start input : 0 for Trig1 - 1 for Trig2

    chans = [4, 40]
    # enable chans
    for chan in chans:
        enable_ch_inj(chan, range_capa = "low")
        mask_toa(chan, 0)
        mask_tot(chan, 0)

        set_probe_toa(chan, 1) # Enable probe_ToA output
        sel_trigger_toa(chan, ext_trig_number) #Select TDC external start input : 0 for Trig1 - 1 for Trig2

        set_probe_tot(chan, 1) # Enable probe_ToT output
        sel_trigger_tot(chan, ext_trig_number) #Select TDC external start input : 0 for Trig1 - 1 for Trig2

    phase = 14
    bx_offset = -3

    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    print "Setting clock phase to %i" %phase , " with word" , ret

    strobe = 0

    # Trig 1 and Trig 2 offset and length in 320 MHz clock cycles
    trig1_offset = 2
    trig1_length = 40

    trig2_offset = 2
    trig2_length = 25

    if ext_trig_number != 0: ext_trig_number = 1 #boolean style

    if ext_trig_number == 0:
        set_toa_trig1(bx = strobe, offset_320 = trig1_offset, length_320 = trig1_length)
        #set_toa_trig1(0,0,0)
        set_toa_trig2(0,0,0)
    else:
        set_toa_trig2(bx = strobe, offset_320 = trig2_offset, length_320 = trig2_length)
        #set_toa_trig2(0,0,0)
        set_toa_trig1(0,0,0)

    #calib = strobe
    l1a = strobe + 10 + bx_offset + l1a_offset

    c_start = l1a + capture_offset
    ev_size = 40
    c_stop = c_start + ev_size + 1

    print strobe, l1a, c_start, c_stop

    # Fast CMDs
    #set_fcmds(orb = 1e4, l1a = l1a, calib = calib)
    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib()
    set_fcmd_orbit()
    set_fcmd_sync()
    set_fcmd_dump()

    #set_gpio_p(calib, calib +1)
    set_gpio_p(strobe, strobe + 1)
    set_gpio_n(l1a, l1a +1)

    set_capture_window(c_start, c_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    calib_dac = 500

    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all","Calib_dac", phase)

    #outdir = odir + "/calibDAC_%i" %calib_dac
    outdir = odir + "/"
    outdir = os.path.abspath(outdir) + "/"

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    ## selRawData
    select_raw_data(1)

    flush_fifos()
    bash_acq_loop(n_events, 0)

    #print "Fifo full?", check_fifo_full()
    #print("Reading data...")
    #wr_mode = "w" if event == 0 else "a"
    if check_fifo_full():
        for ififo in range(0,6):
            try:
                read_fifo(ififo,outdir + "/", ev_size * n_events)
                continue
            except ValueError:
                print("Error")

    # check fifo is empty
    #print "Fifo full?", check_fifo_full()

    for ch in chans:
        disable_ch_inj(ch)

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
