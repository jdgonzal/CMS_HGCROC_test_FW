#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 50):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/injection/run_" + timestamp #+ "_Vref_Inv77_NoInv25_noTOTTOA"

    print "Output dir:"
    print odir

    l1a_offset = 10
    set_roc_parameter(df_params, "DigitalHalf", "all","L1Offset", l1a_offset)

    phase = 14
    print "Setting clock phase to %i" %phase
    set_roc_parameter(df_params, "Top", "all","Phase", phase)

    # Fast CMDs
    disable_fcmds()

    strobe = 0
    calib = strobe
    calib_stop = calib + 10
    l1a = calib + calib_offset + l1a_offset

    if calib_stop >= l1a: calib_stop = l1a - 1

    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib(calib, calib_stop)
    set_gpio_p(calib, calib_stop)

    # Data capture window
    c_start = l1a + capture_offset
    ev_size = 44
    c_stop = c_start + ev_size + 1
    set_capture_window(c_start, c_stop)

    ## Trigger capture window
    c_trig_start = calib + calib_offset + capture_offset - 5
    c_trig_stop = c_trig_start + 10
    for ififo in range(4):
        set_fifo_capture_window(ififo, c_trig_start, c_trig_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    calib_dacs = [-1] + range(0,2000,10)

    print("Disabling all channels")
    ## disable all channels:
    for chan in range(72):
        #disable_ch_inj(chan)
        set_roc_parameter(df_params, "ch", chan, "LowRange", 0)
        #set_roc_parameter(df_params, "ch", chan, "HighRange", 0)

    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "ExtCtest", 0)

    print("Starting scan")
    #for chan in range(0,72,1):
    for chan in range(0,36,9):
        print 80*"#"
        print "Channel", chan
        chans = [chan, chan+36]
        # enable chans
        for ch in chans:
            set_roc_parameter(df_params, "ch", ch, "LowRange", 1)
            #set_roc_parameter(df_params, "ch", ch, "HighRange", 1)

        for calib_dac in calib_dacs:
            print "Setting calib DAC to", calib_dac
            #set_calib_dac(calib_dac)

            if calib_dac >= 0:
                set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", calib_dac)
                set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)

            else:
                set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", 0)
                set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 0)

            outdir = odir + "/chan_%i/calibDAC_%i" %(chan, calib_dac)
            outdir = os.path.abspath(outdir) + "/"

            if not os.path.exists(outdir):
                os.makedirs(outdir)

            flush_fifos()
            bash_acq_loop(n_events, 0)
            read_fifos([4,5],outdir + "/", data_size)

        print
        ## end calib loop

        # disable channels
        for ch in chans:
            set_roc_parameter(df_params, "ch", ch, "LowRange", 0)
            set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

    ## end chan loop
    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
