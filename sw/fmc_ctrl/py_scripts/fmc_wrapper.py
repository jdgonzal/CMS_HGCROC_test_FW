#!/usr/bin/python
import subprocess
import time

cwd = "../" # dir where fmc_hgroc command is
base_cmd = "./fmc_hgroc "

def exec_cmd(cmd):
    cmds = base_cmd + cmd
    try:
        if ">" in cmds:
            ret = subprocess.check_output(cmds, shell=True, cwd = cwd)
        else:
            ret = subprocess.check_output(cmds.split(), cwd = cwd)
        return ret

    except subprocess.CalledProcessError as e:
        print "Error in command execution"
        print e.output
