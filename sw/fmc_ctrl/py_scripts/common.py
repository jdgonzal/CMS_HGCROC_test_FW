#!/usr/bin/python
import os, subprocess, time

from fmc_wrapper import exec_cmd
from fpga_control import *
#from roc_i2c import *

from hgcroc_i2c_wrapper import *
df_params = load_param_df()

from config import *

daq = DAQ("configs/daq_params.yaml")
#print daq

max_fifo_size = daq.max_fifo_size
idle_word = daq.idle_word # latency wrt L1A
calib_offset = daq.calib_offset # due to ROC align buffer?
align_offset = daq.align_offset # due to ROC align buffer?
l1a_offset = daq.l1a_offset # ROC SC parameter
capture_offset = daq.capture_offset # latency wrt L1A

### ROC GAIN calculator

def compute_cf(value):
    val_enabled_bits = np.array( [1*(((value >> i) & 1) != 0) for i in range(4)])

    cf_vals = np.array([50, 100, 200, 400])
    cf_value = np.sum(cf_vals * val_enabled_bits)

    return cf_value

def set_Cf(value, half = "all"):
    """ Set feedback resistance """

    set_roc_parameter(df_params, "GlobalAnalog", half, "Cf", value)

    cf_value = compute_cf(value)

    return cf_value

def read_Cf(half = "all"):
    """ Set feedback resistance """

    ret = read_roc_parameter(df_params, "GlobalAnalog", half, "Cf")

    if half == "all":
        return [compute_cf(value) for value in ret.values()]
    else:
        return compute_cf(ret)

def compute_rf(value):
    if value == 0: return None

    val_enabled_bits = np.array( [1*(((value >> i) & 1) != 0) for i in range(4)])

    rf_vals = np.array([100, 66.66, 50, 25])
    rf_value = 1/np.sum((1/rf_vals) * val_enabled_bits)

    return rf_value

def set_Rf(value, half = "all"):
    """ Set feedback resistance """

    set_roc_parameter(df_params, "GlobalAnalog", half, "Cf", value)
    rf_value = compute_rf(value)

    return rf_value

def read_Rf(half = "all"):
    """ Set feedback resistance """

    ret = read_roc_parameter(df_params, "GlobalAnalog", half, "Rf")

    if half == "all":
        return [compute_rf(value) for value in ret.values()]
    else:
        return compute_rf(ret)

#### Functions below to be removed!
########################################
########################################

'''
##### ROC basic V settings

def set_bandgap(val = 0xcc):

    set_I2C_param(0x0,0x5,val) # half 0
    set_I2C_param(0x0,0x25,val) # half 1

    return

def set_Vref_inv(val, half = "both"):
    """ Set Vref inv: 8 bits """
    reg = 4

    if half == 0:
        subadds = [40]
    elif half == 1:
        subadds = [296]
    else:
        subadds = [40,296]

    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        set_I2C_param(val_R0, val_R1, val)

    return

def read_Vref_inv(half = "both"):
    """ Set Vref inv: 8 bits """
    reg = 4

    if half == 0:
        subadds = [40]
    elif half == 1:
        subadds = [296]
    else:
        subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        rets.append(ret)

    return rets

def set_Vref_noinv(val, half = "both"):
    """ Set Vref noinv: 8 bits """
    reg = 5

    if half == 0:
        subadds = [40]
    elif half == 1:
        subadds = [296]
    else:
        subadds = [40,296]

    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        set_I2C_param(val_R0, val_R1, val)

    return

def read_Vref_noinv(half = "both"):
    """ Set Vref noinv: 8 bits """
    reg = 5

    if half == 0:
        subadds = [40]
    elif half == 1:
        subadds = [296]
    else:
        subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        rets.append(ret)

    return rets

#### ADC delay

def set_ADCdelay_bits40(val = 0, half = "both"):
    """ Set delay in the ADC for bits 0-4 """
    reg = 13 #register

    # define mask to not touch the other bits
    bit_shift = 2
    mask = 0x1c

    if val > 7: val = 7
    val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param_mask(val_R0, val_R1, val, mask)

    return

def read_ADCdelay_bits40(half = "both"):
    """ Read delay in the ADC for bits 0-4 """
    reg = 13 #register

    # define mask to not touch the other bits
    bit_shift = 2
    mask = 0x1c
    #val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        ret = (ret & mask) >> bit_shift
        rets.append(ret)

    return rets

def set_ADCdelay_bits65(val = 0, half = "both"):
    """ Set delay in the ADC for bits 5-6 """
    reg = 13 #register

    # define mask to not touch the other bits
    bit_shift = 5
    mask = 0xE0

    if val > 7: val = 7
    val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param_mask(val_R0, val_R1, val, mask)

    return

def read_ADCdelay_bits65(half = "both"):
    """ Read delay in the ADC for bits 5-6 """
    reg = 13 #register

    # define mask to not touch the other bits
    bit_shift = 5
    mask = 0xE0

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        ret = (ret & mask) >> bit_shift
        rets.append(ret)

    return rets

def set_ADCdelay_bits87(val = 0, half = "both"):
    """ Set delay in the ADC for bits 7-8 """
    reg = 14 #register

    # define mask to not touch the other bits
    bit_shift = 2
    mask = 0x1C

    if val > 7: val = 7
    val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param_mask(val_R0, val_R1, val, mask)

    return

def read_ADCdelay_bits87(half = "both"):
    """ Read delay in the ADC for bits 7-8 """
    reg = 14 #register

    # define mask to not touch the other bits
    bit_shift = 2
    mask = 0x1C
    #val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        ret = (ret & mask) >> bit_shift
        rets.append(ret)

    return rets

def set_ADCdelay_bits9(val = 0, half = "both"):
    """ Set delay in the ADC for bit 9 """
    reg = 14 #register

    # define mask to not touch the other bits
    bit_shift = 5
    mask = 0xE0

    if val > 7: val = 7
    val = (val << bit_shift) & mask

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param_mask(val_R0, val_R1, val, mask)

    return

def read_ADCdelay_bits9(half = "both"):
    """ Read delay in the ADC for bit 9 """
    reg = 14 #register

    # define mask to not touch the other bits
    bit_shift = 5
    mask = 0xE0

    if half == 0: subadds = [41]
    elif half == 1: subadds = [297]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret, 16)
        ret = (ret & mask) >> bit_shift
        rets.append(ret)

    return rets

###### PLL

def set_pll_config():

    set_I2C_R0(0x80); set_I2C_R1(0x05)
    val = 0x30 #Kill PLL_lock 'only'
    #val = 0x10 #Kill PLL_lock and other digital blocks
    set_I2C_R2(val)

    set_I2C_R0(0x81); set_I2C_R1(0x05)
    val = 0x19 # Enable high capa
    set_I2C_R2(val)

    set_I2C_R0(0x83); set_I2C_R1(0x05)
    val = 0x7F #Max charge pump bias
    set_I2C_R2(val)

    return 1

def set_clk_phase(phase = 0):
    """ Set 40 MHz clock phase offset: 4 bits for 25ns"""

    if (phase > 15) or (phase < 0):
        print("Phase out of bounds!")
        phase = 0

    # bit-shift phase by 1 bit (LSB in reg is enabling the phase-shifting)
    word = (phase << 1) | 0x1

    set_I2C_R0(0x87);
    set_I2C_R1(0x05);
    set_I2C_R2(hex(word))

    return word

def probe_pll(probe = True):
    """ Enable PLL probe output pads (PLL_P/N) for test purpose """

    #Register 7 of top sub block
    val_R0 = 0x87
    val_R1 = 0x05
    val_R2 = read_I2C_param(val_R0, val_R1)
    val = int(val_R2, 16)

    if probe == True:   # Enable PLL probe output
        val = val | 0x80
    else: # False : Disable PLL probe output
        val = val & 0x1F

    set_I2C_param(val_R0, val_R1, val)
    val_R2 = read_I2C_param(val_R0, val_R1)
    return val_R2

def set_PLL_ext_clock(ext = True):
    """ PLL by-pass : Select the external 40 MHz clock as PLL input """

    if ext == True:   # Select external PLL clock input
        set_ext_clock(1) # IPbus register in FW : FMC_Sel_ck_ext goes HIGH
    else: # False : # Select internal PLL clock input decoded from fast commands stream
        set_ext_clock(0) # IPbus register in FW : FMC_Sel_ck_ext goes LOW

    #Register 3 of top sub block
    val_R0 = 0x83
    val_R1 = 0x05
    val_R2 = read_I2C_param(val_R0, val_R1)
    val = int(val_R2, 16)

    if ext == True:   # Enable PLL probe output
        val = val | 0x80
    else: # False : Disable PLL probe output
        val = val & 0x7F

    set_I2C_param(val_R0, val_R1, val)
    val_R2 = read_I2C_param(val_R0, val_R1)
    return val_R2

### Probe TOA/TOT

def mask_toa(channel, val):
    reg = 3
    bit_shift = 0
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_toa_mask(channel):
    reg = 3
    bit_shift = 0
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

def mask_tot(channel, val):
    reg = 4
    bit_shift = 5
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_tot_mask(channel):
    reg = 4
    bit_shift = 5
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

## probe TOA/TOT

def set_probe_toa(channel, val):
    reg = 4
    bit_shift = 7
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_probe_toa(channel):
    reg = 4
    bit_shift = 7
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

def set_probe_tot(channel, val):
    reg = 4
    bit_shift = 6
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_probe_tot(channel):
    reg = 4
    bit_shift = 6
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

def sel_trigger_toa(channel, val):
    reg = 3
    bit_shift = 6
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_sel_trigger_toa(channel):
    reg = 3
    bit_shift = 6
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

def sel_trigger_tot(channel, val):
    reg = 4
    bit_shift = 4
    max_val = 0x1

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param_mask(val_R0, val_R1, val_R2, mask)
    return

def read_sel_trigger_tot(channel):
    reg = 4
    bit_shift = 4
    max_val = 0x1
    mask = max_val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    print val_R0, val_R1, mask

    ret = read_I2C_param(val_R0, val_R1)
    print ret
    return

def set_L1_offset(offset):
    """ Set L1 offset """

    # half 0
    set_I2C_R0(0x62); set_I2C_R1(0x05)
    set_I2C_R2(offset)

    # half 1
    set_I2C_R0(0x62); set_I2C_R1(0x25)
    set_I2C_R2(offset)

    return



### Select Raw Data
def select_raw_data(val = 1, half = "both"):
    """ Select RAW data (12b TOT) """
    reg = 0 #register

    # define mask to not touch the other bits (default is 0x0f)
    mask = 0x0e
    val = val | mask

    if half == 0: subadds = [43]
    elif half == 1: subadds = [299]
    else: subadds = [43,299]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param(val_R0, val_R1, val)

    return

### CHANNEL - WISE

### Channel 5b pedestal fine adjustment

def set_channel_ped(channel, val):
    reg = 3
    bit_shift = 1
    max_val = 0x1F

    if val > max_val: val = max_val

    # create payload
    val_R2 = val << bit_shift

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param(val_R0, val_R1, val_R2)
    return

def read_channel_ped(channel):
    reg = 3
    bit_shift = 1
    mask = 0x1F

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    val_R2 = read_I2C_param(val_R0, val_R1)
    val = int(val_R2, 16)
    val = (val >> bit_shift) & mask

    return val

### Channel ON/OFF

def enable_channel(channel):
    # register 4
    reg = 4
    # create payload
    val_R2 = 0

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param(val_R0, val_R1, val_R2)
    return

def disable_channel(channel):
    # register 4
    reg = 4
    # create payload
    val_R2 = 1

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    set_I2C_param(val_R0, val_R1, val_R2)
    return

### INJECTION

## For now fixed channels!

def enable_ch_inj(channel, range_capa = "low"):

    # low/high range capa are in register 4: bits 1/2
    reg = 4
    if range_capa == "low":
        capa_bit = 1
    elif range_capa == "high":
        capa_bit = 2

    # create payload
    val_R2 = hex(0x1 << capa_bit)

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)

    val_R0, val_R1 = get_par_addr(sub_add, reg)

    #print "Setting for chan %i, r0 %s, r1 %s, r2 %s" %(channel, val_R0, val_R1, val_R2)
    set_I2C_R0(val_R0);
    set_I2C_R1(val_R1);
    set_I2C_R2(val_R2)

    return

def disable_ch_inj(channel):

    # low/high range capa are in register 4: bits 1/2
    reg = 4
    # create payload
    val_R2 = 0

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)

    val_R0, val_R1 = get_par_addr(sub_add, reg)

    #print "Setting for chan %i, r0 %s, r1 %s, r2 %s" %(channel, val_R0, val_R1, val_R2)
    set_I2C_R0(val_R0);
    set_I2C_R1(val_R1);
    set_I2C_R2(val_R2)

    return

def read_ch_inj(channel):

    # low/high range capa are in register 4: bits 1/2
    reg = 4

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)

    val_R0, val_R1 = get_par_addr(sub_add, reg)

    #print "Reading for chan %i, r0 %s, r1 %s" %(channel, val_R0, val_R1)
    set_I2C_R0(val_R0);
    set_I2C_R1(val_R1);
    #set_I2C_R2(val_R2)
    read_I2C_R2()

    return

def disable_calib():
    val = 0x0

    ## set DAC in 2 half
    set_I2C_param(0x07,0x25,val)

    ## set DAC in 1 half
    set_I2C_param(0x07,0x05,val)

    return

def set_calib_dac(calib_dac = 0, ctest_bit = 0x40):

    if calib_dac == -1:
        # don't enable the ctest_bit and set DAC to 0
        ctest_bit = 0
        calib_dac = 0

    #print hex(calib_dac)
    calib_dac_lsb = calib_dac & 0xff
    calib_dac_msb = (calib_dac >> 8) & 0xf

    #print calib_dac_lsb, calib_dac_msb

    ## int_c_test bit
    #ctest_bit = 0x40 # IntCtest enabled
    #ctest_bit = 0xf0

    calib_dac_msb = calib_dac_msb | ctest_bit

    #print calib_dac_lsb, calib_dac_msb
    #print hex(calib_dac_lsb), hex(calib_dac_msb)

    ## set DAC in 1 half
    # LSB in reg 6
    set_I2C_param(0x06,0x25,hex(calib_dac_lsb))
    # MSB in reg 7
    set_I2C_param(0x07,0x25,hex(calib_dac_msb))

    ## set DAC in 1 half
    # LSB in reg 6
    set_I2C_param(0x06,0x05,hex(calib_dac_lsb))
    # MSB in reg 7
    set_I2C_param(0x07,0x05,hex(calib_dac_msb))

    return

### PROBE DC Points

def read_probe_dc1(half = "both"):
    """ Probe DC1 : 4 bits """
    reg = 8 # register

    mask = 0x0f

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret,16) & mask
        rets.append(ret)

    return rets

def set_probe_dc1(val, half = "both"):
    """ Probe DC1 : 4 bits """
    reg = 8 # register

    mask = 0x0f
    # force value is within range:
    #val = val & mask

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param(val_R0, val_R1, val)
    return

def read_probe_dc2(half = "both"):
    """ Probe DC2 : 4 bits """
    reg = 8 # register
    mask = 0xf0

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = (int(ret,16) & mask >> 4)
        rets.append(ret)

    return rets

def set_probe_dc2(val, half = "both"):
    """ Probe DC2 : 4 bits """
    reg = 8 # register

    mask = 0xf0
    # force value is within range:
    #val = val & mask

    # bit shift to 4 MSB
    val = val << 4

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = set_I2C_param(val_R0, val_R1, val)
    return

#### THRESHOLDS

#### TOT ####

def read_tot_threshold(half = "both"):
    """ TOT Threshold is 8 bits """
    reg = 2 # register

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret,16)
        rets.append(ret)

    return rets

def set_tot_threshold(thr, half = "both"):
    """ TOT Threshold is 8 bits """
    reg = 2 # register

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    val_R2 = (thr & 0xff) # make sure only 8 bits used

    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        set_I2C_param(val_R0, val_R1, val_R2)
    return

def read_tot_tune_threshold(channel_id):
    """ TOT local DAC Tune Threshold is 5 bits """
    reg = 2 # register

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel_id)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    ret = int(ret,16) / 2 # /2 for bitshift to the right
    return ret

def set_tot_tune_threshold(channel_id, thr):
    """ TOT local DAC Tune Threshold is 5 bits """
    reg = 2 # register

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel_id)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    val_R2 = read_I2C_param(val_R0, val_R1)
    val = (int(val_R2, 16) & 0x01) | ((thr & 0x1F) * 2) # make sure only 5 bits used

    set_I2C_param(val_R0, val_R1, val)
    return

#### TOA ####

def read_toa_threshold(half = "both"):
    """ TOA Threshold is 8 bits """
    reg = 3 # register

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    rets = []
    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        ret = read_I2C_param(val_R0, val_R1)
        ret = int(ret,16)
        rets.append(ret)

    return rets

def set_toa_threshold(thr, half = "both"):
    """ TOA Threshold is 8 bits """
    reg = 3 # register

    if half == 0: subadds = [40]
    elif half == 1: subadds = [296]
    else: subadds = [40,296]

    val_R2 = (thr & 0xff) # make sure only 8 bits used

    for subadd in subadds:
        val_R0, val_R1 = get_par_addr(subadd, reg)
        set_I2C_param(val_R0, val_R1, val_R2)
    return

def read_toa_tune_threshold(channel_id):
    """ TOA local DAC Tune Threshold is 5 bits """
    reg = 1 # register

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel_id)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    ret = int(ret,16) / 2 # /2 for bitshift to the right
    return ret

def set_toa_tune_threshold(channel_id, thr):
    """ TOA local DAC Tune Threshold is 5 bits """
    reg = 1 # register

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel_id)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    # make sure only 5 bits used
    val_R2 = (thr & 0x1f) * 2 # *2 for bit shift of 1 to the left

    set_I2C_param(val_R0, val_R1, val_R2)
    return

def set_toa_ext(channel_id, value):
    """ Select trigger input for the given ToA TDC channel.  """
    """ Channel id from 0 to 71. CM and calib channel not accessible in this function """
    """ Value = 0 for discriminator output. 1 for Trig1 and 2 for Trig2 """
    reg = 3 # register

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel_id)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    val_R2 = read_I2C_param(val_R0, val_R1)
    val = int(val_R2, 16)

    if value == 1:   # Masked and Trig1
        val = (val | 0x01) & 0xBF
    elif value == 2: # Masked and Trig2
        val = val | 0x41
    else: # 0 or anything else : Not masked so do not care if Trig1 or Trig2
        val = val & 0xBE

    print "ToA ext : avant = " + val_R2 + "    R0/R1 = " + val_R0 + " / " + val_R1
    set_I2C_param(val_R0, val_R1, val)

    val_R2 = read_I2C_param(val_R0, val_R1)
    print "ToA ext : apres = " + val_R2
    return

def read_toa_probe(channel):
    # register 4
    reg = 4

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    ret = (int(ret,16) & 0x80) / 128 # /128 for bitshift from bit 7
    return ret

def set_toa_probe(channel, bitvalue):
    # register 4
    reg = 4

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    val_R2 = read_I2C_param(val_R0, val_R1)
    if bitvalue == 0:
        val = int(val_R2, 16) & 0x7F
    else:
        val = int(val_R2, 16) | 0x80

    print "ToA probe : avant = " + val_R2 + "    R0/R1 = " + val_R0 + " / " + val_R1
    set_I2C_param(val_R0, val_R1, val)

    val_R2 = read_I2C_param(val_R0, val_R1)
    print "ToA probe : apres = " + val_R2
    return

def read_tot_probe(channel):
    # register 4
    reg = 4

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    ret = read_I2C_param(val_R0, val_R1)
    ret = (int(ret,16) & 0x40) / 64 # /64 for bitshift from bit 6
    return ret

def set_tot_probe(channel, bitvalue):
    # register 4
    reg = 4

    ## get channel sub_addr
    sub_add = get_chan_subadd(channel = channel)
    val_R0, val_R1 = get_par_addr(sub_add, reg)

    val_R2 = read_I2C_param(val_R0, val_R1)
    if bitvalue == 0:
        val = int(val_R2, 16) & 0xBF
    else:
        val = int(val_R2, 16) | 0x40

    print "ToT probe : avant = " + val_R2 + "    R0/R1 = " + val_R0 + " / " + val_R1
    set_I2C_param(val_R0, val_R1, val)

    val_R2 = read_I2C_param(val_R0, val_R1)
    print "ToT probe : apres = " + val_R2
    return
'''
