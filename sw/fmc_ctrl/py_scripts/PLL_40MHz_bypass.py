#!/usr/bin/python
from common import *

def set_bypass(bypass = 1):

##### Top sub block parameters list 

    # EN_LOCK_CONTROL
    # ERROR_LIMIT_SC
    # Sel_PLL_Locked
    # PllLockedSc
    # OrbitSync_sc
    
    # EN_PLL
    # DIV_PLL_A
    # DIV_PLL_B
    # EN_HIGH_CAPA
    # EN_REF_BG
    # VOUT_INIT_EN
    
    # VOUT_INIT_EXT_EN
    # VOUT_INIT_EXT_D
    
    # FOLLOWER_PLL_EN
    # BIAS_I_PLL_D
    # Sel_40M_ext
    
    # EN1_probe_pll
    # EN2_probe_pll
    # EN2_probe_pll
    # EN-pE0_probe_pll
    # EN-pE1_probe_pll
    # EN_pE2_probe_pll
    # S0_probe_pll
    # S1_probe_pll
    
    # EN1
    # EN2
    # EN3
    # EN-pE0
    # EN-pE1
    # EN-pE2
    # S0
    # S1
    
    # Sel_ReSync_fcmd
    # Sel_L1_fcmd
    # Sel_STROBE_fcmd
    # Sel_OrbitSync_fcmd
    
    # En_PhaseShift
    # Phase
    # En_pll_ext
    
    
    ext = 0 #value initialization
    
    if bypass != 0: ext = 1 # else: ext = 0
    
    set_ext_clock(ext) #IPbus register to set FMC_Sel_ck_ext signal
    set_roc_parameter(df_params, "Top", 0, "Sel_40M_ext", ext) #ROC register in Top sub-block

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        bypass_value = int(sys.argv[1])
        print("## PLL bypass : %i" %bypass_value)
        set_bypass(bypass_value)
    else:
        set_bypass()

if __name__ == "__main__":

    main()
