#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 10, suffix = ""):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/shaper_vref_scan/run_" + timestamp + suffix
    odir = os.path.abspath(odir) + "/"

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    strobe = 0
    l1a = strobe + l1a_offset

    ev_size = 40
    c_start = l1a + capture_offset
    c_stop = c_start + ev_size + 1

    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    # Fast CMDs
    #set_fcmds(orb = 1e4, l1a = l1a, calib = 1e4)
    disable_fcmds()

    set_fcmd_l1a(l1a, l1a+1)
    set_capture_window(c_start, c_stop)

    vref_pairs = [(range(0,1024,5),range(0,1024,5))]

    for vref_pair in vref_pairs:
        vrefs_inv, vrefs_noinv = vref_pair

        for vref_inv in vrefs_inv:
            print("Setting Vref inv value to %i" %vref_inv)
            #set_Vref_inv(vref_inv)
            set_roc_parameter(df_params, "ReferenceVoltage", "all","Inv_vref", vref_inv)

            for vref_noinv in vrefs_noinv:
                print("Setting Vref no inv value to %i" %vref_noinv)
                #set_Vref_noinv(vref_noinv)
                set_roc_parameter(df_params, "ReferenceVoltage", "all","Noinv_vref", vref_noinv)

                ### Take data
                outdir = odir + "/VrefInv_%03d/VrefNoInv_%03d" %(vref_inv, vref_noinv)
                outdir = os.path.abspath(outdir) + "/"

                if not os.path.exists(outdir):
                    os.makedirs(outdir)

                flush_fifos()
                bash_acq_loop(n_events, 0)
                read_fifos([4,5], outdir + "/", data_size)

    return

def main():

    n_events = 10
    suffix = ""

    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    acq(n_events, suffix)

if __name__ == "__main__":

    main()
