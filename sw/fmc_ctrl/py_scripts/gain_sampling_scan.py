#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def samp_scan(odir = "./data/", offsets = [0], phases = [0], n_events = 1):

    # config DAQ signals
    #config_daq_controller() #by defaul to 0

    ev_size = 40

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    strobe = 0
    calib = strobe
    calib_stop = calib + calib_offset - 1

    disable_fcmds()

    set_fcmd_calib(calib, calib_stop)
    set_gpio_p(calib, calib_stop)

    for bx_offset in offsets:

        calib = strobe
        l1a = calib + calib_offset + l1a_offset + bx_offset

        c_start = l1a + capture_offset
        c_stop = c_start + ev_size + 1

        # Fast CMDs
        set_fcmd_l1a(l1a, l1a+1)
        set_capture_window(c_start, c_stop)

        print("Setting BX offset for L1A to %i" %bx_offset)

        for phase in phases:
            set_roc_parameter(df_params, "Top", "all","Phase", phase)
            #print "Setting clock phase to %i" %phase , " with word" , ret

            outdir = odir + "/bxOff_%i_phase_%i" %(bx_offset,phase)
            outdir = os.path.abspath(outdir) + "/"

            #print outdir

            if not os.path.exists(outdir):
                os.makedirs(outdir)

            flush_fifos()
            bash_acq_loop(n_events, 0)
            read_fifos([4,5],outdir + "/", data_size)

        ## end phase loop
    ## end offset loop


def acq(n_events = 10, suffix = ""):

    ## Set DAC values
    calib_dac = 500
    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "ExtCtest", 0)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", calib_dac)

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/sampling_scan/run_" + timestamp + "_calDAC_%i"%calib_dac + suffix

    print "Output dir:"
    print odir

    print("Disabling all channels")
    ## disable all channels:
    for chan in range(72):
        #disable_ch_inj(chan)
        set_roc_parameter(df_params, "ch", chan, "LowRange", 0)
        #set_roc_parameter(df_params, "ch", chan, "HighRange", 0)

    l1a_offset = 10
    set_roc_parameter(df_params, "DigitalHalf", "all","L1Offset", l1a_offset)

    bx_offsets = range(0,3)
    phases = range(16)

    ## set Cf_comp
    #cf_comp = 200 #code = 2
    cf_comp = 0 #code = 0
    set_roc_parameter(df_params, "GlobalAnalog", "all", "Cf_comp", 0)

    cf_codes = range(0,16,1)
    rf_codes = range(1,15,1)

    rc_ref = 33.3*400
    rf_cf_vals = []

    '''
    for cf_code in cf_codes:
        cf_val = compute_cf(cf_code)

        for rf_code in rf_codes:
            rf_val = compute_rf(rf_code)

            if abs(rf_val * cf_val - rc_ref) / rc_ref < 0.2:
                rf_cf_vals.append((cf_val, int(rf_val)))

    print rf_cf_vals
    exit(0)
    '''

    for chan in range(0,36,1):
        chans = [chan, chan+36]
        #chans = range(72)
        # enable chans
        print("Enabling channels:")
        print(chans)

        for ch in chans:
            set_roc_parameter(df_params, "ch", ch, "LowRange", 1)
            #set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

            # mask TOT
            set_roc_parameter(df_params, "ch", chan, "Mask_tot", 1)

        for cf_code in cf_codes:
            cf_val = set_Cf(cf_code)
            cf_val += cf_comp

            #print "# Cf: code, value:", cf_code, cf_val
            print "# Cf: value:", cf_val

            for rf_code in rf_codes:
                rf_val = compute_rf(rf_code)

                if abs(rf_val * cf_val - rc_ref) / rc_ref < 0.8:

                    set_Rf(rf_code)

                    #print "## Rf: code, value:", rf_code, rf_val
                    print "## Rf: value:", rf_val

                    outdir = odir + "/chan_%i/Cf_%i_Rf_%.2f" %(chan, cf_val, rf_val)
                    #print outdir

                    samp_scan(outdir, bx_offsets, phases, n_events)
                    #break

            ## end Rf loop
        ## end Cf loop

        # disable channels
        for ch in chans:
            #disable_ch_inj(ch)
            set_roc_parameter(df_params, "ch", ch, "LowRange", 0)
            set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

    ## end chan loop
    return

def main():

    n_events = 1
    suf = ""
    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)

    if len(sys.argv) > 2:
        suf = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suf)

    acq(n_events, suf)

if __name__ == "__main__":

    main()
