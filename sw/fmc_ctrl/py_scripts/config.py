#!/usr/bin/python
import yaml

from hgcroc_i2c_wrapper import *

### generic Config class
class Config:

    def __init__(self):
        #self.conf_fname = conf_fname
        #self.param_names = []
        pass

    def load_config(self, conf_fname):
        self.conf_fname = conf_fname

        # read config file
        with open(conf_fname) as f:
            params = yaml.load(f, Loader=yaml.FullLoader)

            #self.param_names += params.keys()

            for param, value in params.iteritems():
                setattr(self, param, value)

            print("Loaded parameters from %s"%conf_fname )
            #print self #params.keys()

        return

    def __init__(self, conf_fname):
        self.conf_fname = conf_fname
        #self.param_names = []

        self.load_config(conf_fname)

    def get_params(self):
        params = {}
        #for param in self.param_names:
        for param in self.__dict__.keys():
            params[param] = getattr(self, param)

        return params

    def __repr__(self):
        params = self.get_params()
        par_str = "Key:\tvalue\n"
        par_str += "\n".join([(str(key) + ":\t" + str(val)) for key, val in params.iteritems()])

        return par_str #params.__str__

    def print_param_names(self):
        print("Parameters:")
        #print(self.param_names)
        print(self.__dict__.keys())

        return

    def save_config(self, conf_fname = None):
        if conf_fname is None: conf_fname = self.conf_fname
        if conf_fname == self.conf_fname:
            print("Updating parameters in %s" %conf_fname)
        else:
            print("Writing parameters to %s" %conf_fname)

        params = self.get_params()

        # read config file
        with open(conf_fname, "w") as f:
            yaml.dump(params, f, sort_keys=True)

        return conf_fname
## End of Config class

## DAQ class
class DAQ(Config):
    def __init__(self, conf_fname):
        print("Creating DAQ object")
        Config.__init__(self, conf_fname)

## End of DAQ class

## ROC class
class ROC(Config):
    '''
    def __init__(self, conf_fname):
        print("Creating ROC object")
        Config.__init__(self, conf_fname)
    '''

    def load_bitmap(self, bitmap_fname = None):
        if bitmap_fname is None: bitmap_fname = self.bitmap_fname

        self.bitmap = load_param_df(bitmap_fname)

    def __init__(self, conf_fname):
        print("Creating ROC object")
        Config.__init__(self, conf_fname)

        if hasattr(self, "bitmap_fname"):
            self.load_bitmap()

    # overwrite get_params to exclude bitmap
    def get_params(self):
        params = {}
        #for param in self.param_names:
        for param in self.__dict__.keys():
            if param == "bitmap": continue
            params[param] = getattr(self, param)

        return params

    def add_sc_param(self, block, blockId, param, value):

        if not hasattr(self, "sc"):
            print("ROC has no SC param list defined!")
            return 0

        if block not in self.sc: self.sc[block] = {}
        if blockId not in self.sc[block]: self.sc[block][blockId] = {}

        self.sc[block][blockId][param] = value

        return 1

    def set_parameter(self, block, blockId, param, value = 0):
        #self.add_sc_param(block, blockId, param, value)
        return set_roc_parameter(self.bitmap, block, blockId, param, value)

    def read_parameter(self, block, blockId, param):
        return read_roc_parameter(self.bitmap, block, blockId, param)

    def config_sc_params(self):
        for block in self.sc:
            for blockId in self.sc[block]:
                for param, value in self.sc[block][blockId].iteritems():
                    print "Setting parameter", block, blockId, param, value
                    self.set_parameter(block, blockId, param, value)

## End of ROC class

if __name__ == "__main__":

    '''
    daq = DAQ("configs/daq_params.yaml")

    print daq.get_params()
    print daq.l1a_offset
    print daq.__dict__.keys()

    daq.new_param = 1234

    daq.link_delays = [0,1,2,3,4,5,6]
    daq.link_bitpos = [0,0,0,0,0,0]

    print daq
    daq.save_config("configs/test.yaml")
    '''

    roc = ROC("configs/roc_config.yaml")
    print roc

    roc.add_sc_param("ReferenceVoltage","all","LowRange", 1)
    roc.add_sc_param("ch",1,"Sel_trigger_toa", 1)
    roc.add_sc_param("ch",1,"Sel_trigger_tot", 1)

    roc.set_parameter("ch", "1-20", "LowRange", 1)
    roc.set_parameter("ch", "30-40", "HighRange", 1)

    roc.config_sc_params()

    roc.save_config("configs/roc_test.yaml")
