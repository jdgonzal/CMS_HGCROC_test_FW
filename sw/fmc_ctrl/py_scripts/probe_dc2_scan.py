#!/usr/bin/python
from common import *

import sys,time

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

def acq():

    probe_ids = range(15)

    vals = []

    for id in probe_ids:

        print "Setting probeDC2 ", id, bin(id)
        set_probe_dc2(id)

        val = raw_input("Enter value: ")
        val = float(val)
        vals.append(val)

    print vals

    '''
    f = plt.figure(figsize = (10,8))
    plt.plot(calib_dacs, vals, "o-")

    plt.xlabel("Calib DAC")
    plt.ylabel("Voltage [mV]")
    plt.savefig("calib_dac_lin.pdf")
    '''

    set_probe_dc2(0)

    return

def main():

    acq()

if __name__ == "__main__":

    main()
