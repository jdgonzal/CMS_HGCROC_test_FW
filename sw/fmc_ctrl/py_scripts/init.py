#!/usr/bin/python
import sys
from common import *

def main(roc_conf_fname = "configs/roc_config.yaml"):

    check_fw_version()

    #daq = DAQ("configs/daq_params.yaml")
    roc = ROC(roc_conf_fname)

    # set delays and bitpos
    set_link_delays(daq.link_delays)
    set_bit_pos(daq.link_bitpos)

    # Disable all fast CMDs
    disable_fcmds()

    # reset ROC digital part
    reset_roc_digital()

    # flush FPGA FIFOs
    flush_fifos()

    # ROC config
    reset_i2c_params()

    # Configure all parameters from config file
    roc.config_sc_params()

    # read IDLE frame pattern
    idle_frame = roc.read_parameter("DigitalHalf", 0,"IdleFrame")
    print("Idle frame: " + hex(idle_frame))

    return

if __name__ == "__main__":

    if len(sys.argv) > 1:
        fname = sys.argv[1]
        print("Using ROC config file: %s" %fname)
        main(fname)
    else:
        main()
