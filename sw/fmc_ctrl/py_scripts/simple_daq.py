#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 1, suffix = ""):

    ev_size = 44

    n_l1a = 1 # number of consecutive L1A (max for same Orbit is 13)
    if n_events < n_l1a:
        print(80*"#")
        print("# Nevents requested lower than #L1A: using single L1A")
        print(80*"#")
        n_l1a = 1

    n_events /= n_l1a

    # Configure fast CMDs
    disable_fcmds()

    strobe = 0
    orbit = strobe
    set_fcmd_orbit(orbit, orbit + 1)

    l1a_offset = 10
    set_roc_parameter(df_params, "DigitalHalf", "all","L1Offset", l1a_offset)

    l1a_start = orbit + l1a_offset #- 1
    l1a_stop = l1a_start + 1

    if n_l1a > 1:
        l1a_stop += capture_offset
        l1a_stop += (ev_size - 1) * (n_l1a - 1)
    #print l1a_start, l1a_stop

    set_fcmd_l1a(l1a_start, l1a_stop)

    # Data capture
    c_start = l1a_start + capture_offset
    c_stop = c_start + (ev_size - 1) * n_l1a
    set_capture_window(c_start, c_stop)
    #set_gpio_p(l1a_start, l1a_stop)
    set_gpio_p(c_start, c_stop)

    #print l1a_start, l1a_stop, c_start, c_stop

    '''
    ## trigger readout
    tp_link_offset = 7
    tp_c_start = orbit + tp_link_offset
    tp_c_stop = tp_c_start + 10

    tp_ev_size = 44
    tp_data_size = n_events * tp_ev_size

    set_fifo_capture_window(0, tp_c_start, tp_c_stop)
    '''

    data_size = (ev_size - 1) * n_events * n_l1a

    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
        data_size = (ev_size - 1) * n_events


    # Create output folder
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/acq/run_" + timestamp + suffix
    odir = os.path.abspath(odir) + "/"
    if not os.path.exists(odir): os.makedirs(odir)
    print "Output dir:"
    print odir

    ## Acquisition

    flush_fifos()
    # check fifo is empty
    print "Fifo content:", bin(check_fifo_content())
    print ("Taking data...")

    bash_acq_loop(n_events, 0)

    print "Fifo content:", bin(check_fifo_content())
    print("Reading data...")

    read_fifos(range(6),odir + "/", data_size)
    #read_fifos(range(4),odir + "/", tp_data_size)
    #read_fifos([4,5],odir + "/", data_size)

    # check fifo is empty
    print "Fifo content:", bin(check_fifo_content())

    return

def main():

    n_events = 1
    suffix = ""

    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    acq(n_events, suffix)

if __name__ == "__main__":

    main()
