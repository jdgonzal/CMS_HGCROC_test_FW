import sys

import numpy as np
import pandas as pd

from fmc_wrapper import exec_cmd

### I2C

## single register operations
def read_I2C_register(reg_id = 0, chip_id = 5):

    i2c_addr = hex((chip_id << 3) + reg_id)

    cmd = " -J " + i2c_addr
    ret = exec_cmd(cmd)

    if "I2C error" in ret:
        print "## Error in reading!"
        print ret
        print "## Trying again:"
        ret = read_I2C_register(reg_id, chip_id)
        print ret

        return ret

    ret = ret.split("\n")[-2]
    ret = int(ret, 16)

    return ret

def set_I2C_register(reg_id = 0, value = 0, chip_id = 5):

    i2c_addr = hex((chip_id << 3) + reg_id)

    cmd = " -J " + i2c_addr + "=" + str(value)
    ret = exec_cmd(cmd)

    if "I2C error" in ret:
        print "## Error in writing!"
        print ret
        print "## Trying again:"
        ret = set_I2C_register(reg_id, value, chip_id)
        print ret

    return ret

## multi-register operations
def read_I2C_parameter(val_r0, val_r1, chip_id = 5):
    set_I2C_register(0, val_r0, chip_id)
    set_I2C_register(1, val_r1, chip_id)
    ret = read_I2C_register(2, chip_id)
    return ret #int(ret, 16)

def set_I2C_parameter(val_r0, val_r1, val_r2, chip_id = 5):

    set_I2C_register(0, val_r0, chip_id)
    set_I2C_register(1, val_r1, chip_id)
    ret = set_I2C_register(2, val_r2, chip_id)

    return ret

## masked setting of register
def set_I2C_parameter_masked(val_r0, val_r1, value, mask, chip_id = 5):

    # read current value
    cur_val = read_I2C_parameter(val_r0, val_r1)
    inv_mask = 0xff - mask

    cur_val = cur_val & inv_mask # bits that should not change

    # provided param is in correct place
    val_r2 = cur_val + value

    ret = set_I2C_parameter(val_r0, val_r1, val_r2, chip_id)

    return ret

def set_dummy_I2C(r0,r1,r2):
    print r0,r1,r2

def read_dummy_I2C(r0,r1, val = "0xff"):
    return int(val, 16)

#### Get I2C block, blockid and parameter list
def get_block_list(df):
    return list(df.reset_index().SubBlock.unique())

def get_blockid_counts(df):
    return df.reset_index().groupby("SubBlock").BlockID.max() + 1

def get_blockid_list(df, block):
    df2 = df.reset_index()
    sel = df2.SubBlock == block
    return sorted(list(df2[sel].BlockID.unique()))

def get_param_list(df, block):
    df2 = df.reset_index()
    sel = df2.SubBlock == block
    return list(df2[sel].parameter.unique())

### Using blockided df
def get_param_nbits(df, block, param):
    return len(df.loc[(block, 0, param)])

def get_param_max(df, block, param):
    return 2**len(df.loc[("DigitalHalf", 0, "L1Offset")]) - 1

def get_param_default(df, block, blockid, name):
    """ Compute parameter value from df """

    blockid = int(blockid)
    print (block, blockid, name)

    df_sel = df.loc[(block, blockid, name)]
    bit_values = df_sel.apply(lambda x: x.Default << x.param_bit, axis = 1)

    return np.sum(bit_values)

def get_I2C_reg_val(sub_addr, reg):
    address = (sub_addr << 5) + reg
    val_R0 = address & 0xff
    val_R1 = (address >> 8) & 0xff

    return hex(val_R0), hex(val_R1)
    #return val_R0, val_R1

def set_roc_parameter(df, block, blockid, name, value = 0):
    """ Set ROC parameter """

    # expand blockIds if not a single value given
    if not isinstance(blockid, int):
        if blockid == "all":
            # Set param for all blockIDs matching subblock name
            b_ids = get_blockid_list(df, block)
        elif "," in blockid:
            # Split into list
            b_ids = [int(bid) for bid in blockid.split(",")]
        elif "-" in blockid:
            # Create range of ids
            limits = [int(bid) for bid in blockid.split("-")]
            b_ids = range(limits[0], limits[1] + 1)
        else:
            return "Unknown blockId", blockid

        for b_id in b_ids:
            #print("Setting blockID %i" %b_id)
            set_roc_parameter(df, block, b_id, name, value)
        return

    df_par = df.loc[(block, blockid, name)]
    # compute number of bits
    nbits = len(df_par)

    ## check value is within range:
    max_val = 2**nbits

    if value > max_val:
        print("Warning: value %i out of range %i" %(value, max_val))
        return None

    ## group by SubAddress and register
    gb = df_par.groupby(["SubAddress","Register"])
    # parameter bit blockides
    param_bits = gb.param_bit.apply(lambda x: x.values).to_dict()
    # bit locations in register
    bit_locations = gb.reg_bit.apply(lambda x: x.values).to_dict()

    reg_adds = param_bits.keys()
    param_parts = {}

    val_enabled_bits = np.array([1*((value & (1 << i)) != 0) for i in range(nbits)])
    #print "Value:", value
    #print "Enabled bits",  val_enabled_bits

    ## Split value in registers
    for reg_add in reg_adds:
        param_bit_positions = param_bits[reg_add]
        reg_bit_positions = bit_locations[reg_add]

        #print 80*"#"
        #print "Param bit /reg positions"
        #print param_bit_positions
        #print reg_bit_positions

        i2c_reg = get_I2C_reg_val(reg_add[0],reg_add[1])

        reg_on_bits = val_enabled_bits[param_bit_positions]
        #print "par On bits in register"
        #print reg_on_bits

        reg_mask = np.sum(2**(reg_bit_positions))
        #print "Mask", reg_mask

        reg_data = np.sum(reg_on_bits * (2**(reg_bit_positions)))
        param_parts[i2c_reg] = reg_data

        #print "Setting parameter", block, blockid, name, value
        #print "Writing reg 0/1/2/mask:",  i2c_reg[0], i2c_reg[1], reg_data, reg_mask
        set_I2C_parameter_masked(i2c_reg[0], i2c_reg[1], reg_data, reg_mask)

    return param_parts

def reset_roc_parameter(df, block, blockid, name):
    """ Reset ROC parameter to default """

    # Set param for all blockIDs matching subblock name
    if blockid == "all":
        b_ids = get_blockid_list(df, block)
        for b_id in b_ids:
            #print("Setting blockID %i" %b_id)
            set_param_default(df, block, b_id, name, value)
        return

    value = get_param_default(df, block, blockid, name)
    return set_roc_parameter(df, block, blockid, name, value)

def read_roc_parameter(df, block, blockid, name):
    """ Read ROC parameter """

    # read param for all blockIDs matching subblock name
    if blockid == "all":
        b_ids = get_blockid_list(df, block)
        rets = {}
        for b_id in b_ids:
            #print("Reading blockID %i" %b_id)
            rets[b_id] = read_roc_parameter(df, block, b_id, name)
        return rets

    # read all params matching subblock name for given blockID
    if name == "all":
        params = get_param_list(df, block)
        rets = {}

        for param in params:
            rets[param] = read_roc_parameter(df, block, blockid, param)
        return rets

    df_par = df.loc[(block, blockid, name)]
    # compute number of bits
    nbits = len(df_par)

    ## group by SubAddress and register
    gb = df_par.groupby(["SubAddress","Register"])
    # parameter bit blockides
    param_bits = gb.param_bit.apply(lambda x: x.values).to_dict()
    # bit locations in register
    bit_locations = gb.reg_bit.apply(lambda x: x.values).to_dict()

    reg_adds = param_bits.keys()
    param_parts = []

    ## Split value in registers
    for reg_add in reg_adds:
        param_bit_positions = param_bits[reg_add]
        reg_bit_positions = bit_locations[reg_add]

        param_bit_values = 2**param_bit_positions

        ## read I2C
        i2c_reg = get_I2C_reg_val(reg_add[0],reg_add[1])

        #ret = read_dummy_I2C(i2c_reg[0],i2c_reg[1], "0xcc")
        ret = read_I2C_parameter(i2c_reg[0],i2c_reg[1])
        #ret = int(ret, 16)
        #print "Read:", ret

        ## determine enabled bits (bit 7 is MSB)
        enabled_bits = np.array([1*((ret & (1 << i)) != 0) for i in range(8)])
        #print "enabled bits:", enabled_bits

        enabled_param_bits = enabled_bits[reg_bit_positions]

        #print "en param bits", enabled_param_bits
        #print param_bit_values

        reg_value = np.sum(enabled_param_bits * param_bit_values)
        #print reg_value

        param_parts.append(reg_value)

    return np.sum(param_parts)

def load_param_df(fname = "configs/HGCROCv2_I2C_Params.csv"):

    #df_params = pd.read_csv("combined/HGCROCv2_I2C_params_v1.csv")
    df_params = pd.read_csv(fname)
    #print df_params.columns

    # set param bit position to 0 for single bit-parameters
    sel = df_params.param_bit == -1
    df_params.loc[sel, "param_bit"] = 0

    ## compute values
    #df_params["bit_value"] = df_params.apply(lambda x: x.Value << x.param_bit, axis = 1)

    ## create multi-blockided df
    df_indx = df_params.sort_values(["SubBlock","BlockID","parameter","param_bit"])
    df_indx = df_indx.set_index(["SubBlock","BlockID","parameter"]).sort_index()

    return df_indx

def main():

    df_params = load_param_df()

    for i in range(2):
        set_roc_parameter(df_params, "ReferenceVoltage", i, "ExtCtest", 1)
        set_roc_parameter(df_params, "ReferenceVoltage", i, "IntCtest", 1)
        set_roc_parameter(df_params, "ReferenceVoltage", i, "Calib_dac", 1000)

    '''
    #print read_roc_parameter(df_params, "DigitalHalf", 0, "L1Offset")
    block = "ReferenceVoltage"
    param = "Tot_vref"

    for i in range(1):
        #print "Read", read_roc_parameter(df_params, "ReferenceVoltage", i, "Calib_dac")
        print "Read", read_roc_parameter(df_params, block, i, param)
        print "Write"
        print set_roc_parameter(df_params, block, i, param, 1023)
        print "Read", read_roc_parameter(df_params, block, i, param)

        #print set_roc_parameter(df_params, "ReferenceVoltage", i, "IntCtest", 1)
        #print set_roc_parameter(df_params, "ReferenceVoltage", i, "ExtCtest", 1)

    '''

    #print get_I2C_reg_val(296, 7)

    '''
    if len(sys.argv) == 3:
        print sys.argv
        sub_addr = int(sys.argv[1])
        reg = int(sys.argv[2])

        print sub_addr, reg

        print get_par_addr(sub_addr, reg)
    else:
        print("Usage: ./roc_i2c sub_address register")
    '''


if __name__ == "__main__":

    main()
