#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 10, suffix = ""):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/ped_scan/run_" + timestamp + suffix
    odir = os.path.abspath(odir) + "/"

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    strobe = 0
    l1a = strobe + l1a_offset

    ev_size = 40
    c_start = l1a + capture_offset
    c_stop = c_start + ev_size + 1

    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    # Fast CMDs
    set_fcmds(orb = 1e4, l1a = l1a, calib = 1e4)
    set_capture_window(c_start, c_stop)

    ped_vals = range(32)

    for ped_val in ped_vals:
        print("Setting pedestal 5b value to %i" %ped_val)

        #for chan in range(72)
        for chan in range(-3,75):
            set_channel_ped(chan, ped_val)

        outdir = odir + "/pedDAC_%i" %(ped_val)
        outdir = os.path.abspath(outdir) + "/"

        if not os.path.exists(outdir):
            os.makedirs(outdir)

        flush_fifos()
        bash_acq_loop(n_events, 0)

        if check_fifo_full():
            for ififo in range(6):
                try:
                    read_fifo(ififo,outdir + "/", data_size)
                except ValueError:
                    print("Error")

    print("Resetting pedestal values")
    #for chan in range(72):
    for chan in range(-3,75):
        set_channel_ped(chan, 0)

    return

def main():

    n_events = 10
    suffix = ""

    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    acq(n_events, suffix)

if __name__ == "__main__":

    main()
