#include "uhal/uhal.hpp"

#include "uhal/tests/tools.hpp"

#include <boost/filesystem.hpp>

#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <cstdlib>
#include <typeinfo>
#include <fcntl.h>
#include <string>
#include <iostream>


using namespace uhal;


uint32_t read_ipbus( uhal::HwInterface *hw, const std::string &node_name )
{
	ValWord<uint32_t> value = hw->getNode( node_name ).read();
	hw->dispatch();

	return value.value();
}

void write_ipbus( uhal::HwInterface *hw, const std::string &node, uint32_t value )
{
	hw->getNode( node ).write( value );
	hw->dispatch();
}

void readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords, void *buffer )
{
	uhal::ValVector<uint32_t> values = hw->getNode( node_name ).readBlock( nwords );
	hw->dispatch();
	for( unsigned i=0; i < nwords; i++ ){
		( (uint32_t*)buffer)[i] = values.at(i);
	}
}

void writeBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, const std::vector<uint32_t> &data )
{
	hw->getNode( node_name ).writeBlock( data );
	hw->dispatch();
}



void dump_drp(HwInterface *hw) {
 ValWord< uint32_t > reg;
 ClientInterface* c = &hw->getClient();
 uint32_t addr = hw->getNode ( "PG_CFG" ).getAddress();

 for(int k=0;k<64;k++) {

  c->write(addr+1, k<<16 );
  c->dispatch();

  reg = c->read(addr+1);
  c->dispatch();
  printf("%d %x\n",k,reg.value()&0xffff);
 }

}

//use mode=1 to configure clock MMCM
void set_delays(HwInterface *hw,uint32_t d[7],int mode) {
 ValWord< uint32_t > reg;
 ClientInterface* c = &hw->getClient();
  uint32_t drp_addr;

 if (mode) drp_addr  = hw->getNode ( "PG_DRP1" ).getAddress(); else
     drp_addr  = hw->getNode ( "PG_DRP" ).getAddress();


 uint32_t cfg_addr = hw->getNode ( "PG_CFG" ).getAddress();
// int j=0;

 ValVector< uint32_t > mem = c->readBlock(drp_addr+6,14);
 c->dispatch();
 if (mode)   c->write(cfg_addr+1,1<<26);	//enable reset of MMCM while reconfiguring
  else   c->write(cfg_addr+1,1<<25);	//enable reset of MMCM while reconfiguring
 std::vector<uint32_t> xx;
 xx.resize ( 14 );

 xx[0]=(mem[0]&0x1fff)|((d[5]&7)<<13);		//cfg_out5
 xx[1]=(mem[1]&0xffc0)|((d[5]>>3)&0x3f);

 xx[2]=(mem[2]&0x1fff)|((d[0]&7)<<13);		//cfg_out0
 xx[3]=(mem[3]&0xffc0)|((d[0]>>3)&0x3f);

 xx[4]=(mem[4]&0x1fff)|((d[1]&7)<<13);		//cfg_out1
 xx[5]=(mem[5]&0xffc0)|((d[1]>>3)&0x3f);

 xx[6]=(mem[6]&0x1fff)|((d[2]&7)<<13);		//cfg_out2
 xx[7]=(mem[7]&0xffc0)|((d[2]>>3)&0x3f);

 xx[8]=(mem[8]&0x1fff)|((d[3]&7)<<13);		//cfg_out3
 xx[9]=(mem[9]&0xffc0)|((d[3]>>3)&0x3f);

 xx[10]=(mem[10]&0x1fff)|((d[4]&7)<<13);	//cfg_out4
 xx[11]=(mem[11]&0xffc0)|((d[4]>>3)&0x3f);

 xx[12]=(mem[12]&0x1fff)|((d[6]&7)<<13);	//cfg_out6
 xx[13]=(mem[13]&0xffc0)|((d[6]>>3)&0x3f);

 c->writeBlock(drp_addr+6,xx);

// printf("updating drp%d:\n",mode);
// for(int k=0;k<14;k++)  printf("%d: %04x %04x  %c\n",k,mem[k]&0xffff,xx[k]&0xffff,((mem[k]&0xffff)!=(xx[k]&0xffff)?'*':' '));

 c->write(cfg_addr+1,0);	//clear resets
 c->dispatch();
}


void pdm_dump_fifo(HwInterface *hw,char *fn) {
 ValWord< uint32_t > reg;
 int fd;
 uint32_t addr = hw->getNode ( "PDM_FIFO" ).getAddress();
 uint32_t d;
 ClientInterface* c = &hw->getClient();
 ValVector< uint32_t > mem = c->readBlock ( addr, 8192+32 ,defs::NON_INCREMENTAL);
 c->dispatch();

 fd=open(fn,O_WRONLY|O_CREAT|O_TRUNC,0777);

 for(int i=0;i<8192+32;i++) {	//is there no better way to do this?
  d=mem[i];
  write(fd,((char*)&d)+3,1);
  write(fd,((char*)&d)+2,1);
  write(fd,((char*)&d)+1,1);
  write(fd,((char*)&d)+0,1);
 }

 close(fd);
}


//use mode=1 to configure clock MMCM
void pdm_mmcm(HwInterface *hw,uint32_t d,uint32_t cfg,int n) {
 ValWord< uint32_t > reg;
 ClientInterface* c = &hw->getClient();
  uint32_t drp_addr;

 drp_addr  = hw->getNode ( "PDM_DRP" ).getAddress();

// ValVector< uint32_t > mem = c->readBlock(drp_addr+6,14);
// c->dispatch();
// int j=0;
// for ( ValVector< uint32_t >::const_iterator i ( mem.begin() ); i!=mem.end(); ++i , ++j ) printf("%d: %x\n",j,*i);

//stop clocks here?

// std::vector<uint32_t> xx;
// xx.resize ( 14 );


 ValWord< uint32_t > reg1,reg2;
 reg1 = c->read(drp_addr+6+0+2*n);
 reg2 = c->read(drp_addr+6+1+2*n);
 c->dispatch();
 printf("was: %x %x\n",reg1.value(),reg2.value());
 reg1=(reg1&0x1fff)|((d&7)<<13);		//cfg_out5
 reg2=(reg2&0xffc0)|((d>>3)&0x3f);
 c->write(drp_addr+6+0+2*n, reg1 );
 c->write(drp_addr+6+1+2*n, reg2 );
 c->dispatch();
 printf("new: %x %x\n",reg1.value(),reg2.value());

/*
   xx[0]=(mem[0]&0x1fff)|((d&7)<<13);		//cfg_out5
   xx[2]=(mem[2]&0x1fff)|((d&7)<<13);		//cfg_out0
   xx[4]=(mem[4]&0x1fff)|((d&7)<<13);		//cfg_out1
   xx[6]=(mem[6]&0x1fff)|((d&7)<<13);		//cfg_out2
   xx[8]=(mem[8]&0x1fff)|((d&7)<<13);		//cfg_out3
   xx[10]=(mem[10]&0x1fff)|((d&7)<<13);	//cfg_out4
   xx[12]=(mem[12]&0x1fff)|((d&7)<<13);	//cfg_out6
*/
}





double gt() {
 struct timeval tv;
 gettimeofday(&tv,0);
 return (double)tv.tv_sec+(double)tv.tv_usec/1000000.0;
}

void main_i2c_init(HwInterface *hw,uint32_t i2c_sel) {
  uint32_t i2c_en,i2c_presc,i2c_settings_value;

  // Disable I2C setting (necessary for prescale change)
  hw->getNode ("main_i2c.settings").write(0);

  printf("i2c_se: %d\n",i2c_sel);
  i2c_en    = 1;
  i2c_presc = 100;
  i2c_settings_value = i2c_en*(1<<15) + i2c_sel*(1<<10) + i2c_presc;
  hw->getNode ("main_i2c.settings").write(i2c_settings_value);
}


uint32_t main_i2c(HwInterface *hw, uint32_t slave_addr, uint32_t wr, uint32_t reg_addr, uint32_t wrdata, uint32_t mem, uint32_t m16b ) {
 uint32_t status;
 uint32_t attempts,max_attempts=100;
 uint32_t comm;
 ValWord< uint32_t > Reg;

 comm    = m16b*(1<<25) + mem*(1<<24) + wr*(1<<23) + slave_addr *(1<<16) + reg_addr *(1<<8) + wrdata;

 hw->getNode ("main_i2c.command").write(comm);
 hw->dispatch();
 hw->getNode ("main_i2c.command").write(comm| 0x80000000);
 hw->dispatch();

 ValWord< uint32_t > reg ;

 status	 = 0;
 attempts = 0;

 while (((status&0xc000000) == 0) && ( attempts <= max_attempts)) {
  Reg = hw->getNode ( "main_i2c.status" ).read();
  hw->dispatch();
  status=Reg.value();
  attempts = attempts +1;
 }

 return status;
}


void hgroc_i2c_init(HwInterface *hw,uint32_t i2c_sel) {
  uint32_t i2c_en,i2c_presc,i2c_settings_value;

  // Disable I2C setting (necessary for prescale change)
  hw->getNode ("hgroc_i2c.settings").write(0);

  printf("i2c_se: %d\n",i2c_sel);
  i2c_en    = 1;
  i2c_presc = 32; // i2c_presc is the number of 31.25MHz cycles per I2C cycle, hence: 31.25/prescale is the I2C clock frequency in MHz
  i2c_settings_value = i2c_en*(1<<15) + i2c_sel*(1<<10) + i2c_presc;
  hw->getNode ("hgroc_i2c.settings").write(i2c_settings_value);
  //printf(i2c_settings_value);
}


uint32_t hgroc_i2c(HwInterface *hw, uint32_t slave_addr, uint32_t wr, uint32_t reg_addr, uint32_t wrdata, uint32_t mem, uint32_t m16b ) {
 uint32_t status;
 uint32_t attempts,max_attempts=100;
 uint32_t comm;
 ValWord< uint32_t > Reg;

 comm    = m16b*(1<<25) + mem*(1<<24) + wr*(1<<23) + slave_addr *(1<<16) + reg_addr *(1<<8) + wrdata;

 hw->getNode ("hgroc_i2c.command").write(comm);
 hw->dispatch();
 hw->getNode ("hgroc_i2c.command").write(comm| 0x80000000);
 hw->dispatch();

 ValWord< uint32_t > reg ;

 status	 = 0;
 attempts = 0;

 while (((status&0xc000000) == 0) && ( attempts <= max_attempts)) {
  Reg = hw->getNode ( "hgroc_i2c.status" ).read();
  hw->dispatch();
  status=Reg.value();
  attempts = attempts +1;
 }

 return status;
}




int main ( int argc,char* argv[] )  {
 char *p,*p2,*p3;
 int  opt;
 int fd,l;
 char buf[65536];

 unsigned int D0,D1;
 std::vector<uint32_t> xx;


 uhal::setLogLevelTo(uhal::Error());

//    uhal::disableLogging();
 ConnectionManager manager("file://../etc/hgroc_connections.xml");
 HwInterface hw = manager.getDevice("hgrocv1_test_1.udp2");
 ValWord< uint32_t > reg;

 while ((opt = getopt(argc, argv, "R:D:r:W:w:l:I:J:")) != -1) {
  switch (opt) {
   case 'R':	//dump register in HEX
    strtok(optarg,":");
    p2=strtok(NULL,"");
    if (p2) { //read multiple entries, e.g. to dump a fifo
      l=strtol(p2,0,0);
      uint32_t *dbuf=(uint32_t*)malloc(4*l);
      readBlock_ipbus(&hw,optarg,l,dbuf);
      for(int i=0;i<l;i++)
	  printf("%x\n",dbuf[i]);
      free(dbuf);
    } else printf("0x%x\n",read_ipbus(&hw,optarg));
    break;

   case 'D':	//dump register in HEX (skip empty words!)
    strtok(optarg,":");
    p2=strtok(NULL,"");
    if (p2) { //read multiple entries, e.g. to dump a fifo
      l=strtol(p2,0,0);
      uint32_t *dbuf=(uint32_t*)malloc(4*l);
      readBlock_ipbus(&hw,optarg,l,dbuf);
      for(int i=0;i<l;i++) {

	  // ignore empty word 0x1234567
	  if (dbuf[i] == 19088743){
	      //printf("Empty\n");
	  }
	  // ignore idle 0xACCCCCCC
	  else if (dbuf[i] == 2899102924){
	      //printf("Empty\n");
	  }
	  // highlight header 0xAAXXXXXX
	  else if ((dbuf[i] >> 24) == 170){
	      //printf("Event\n");
	      printf("%x\n",dbuf[i]);
	  }
	  else{
	      printf("%x\n",dbuf[i]);
	  }
	  //printf("%i\t",i);
	  //printf("%x\n",dbuf[i]);

      }

      free(dbuf);
    } else printf("0x%x\n",read_ipbus(&hw,optarg));
    break;


   case 'r':	//dump register in decimal
    printf("%d\n",read_ipbus(&hw,optarg));
    break;

   case 'W':	//write register
    strtok(optarg,"=");
    p2=strtok(0,"=");

    D0=strtol(p2,0,0);
    write_ipbus(&hw,optarg,D0);
    break;

   case 'l':    //load data from file  <ram_node>:<start_offset>=<filename>
    xx.clear();
    strtok(optarg,":");
    p2=strtok(0,"=");
    p3=strtok(0,"=");

    D0=strtol(optarg,0,0);

    fd=open(p3,O_RDONLY);
    if (fd<0) {
     perror("Error opening file");
     exit(-1);
    }

    l=read(fd,buf,65536);
    if (l<0) {
     perror("Error reading file");
    }
    if (l==65536) {
      fprintf(stderr,"error: file buffer overflow\n");
      exit(-1);
    }

    close(fd);
    p=strtok(buf,"\r");
    p=strtok(0,"\r");
    p=strtok(0,"\r");		//skip the first 2 lines

    while(p) {
      D1=strtol(p,0,16);
      xx.push_back(D1);
//      printf("D1: !%s! %x\n",p,D1);
      p=strtok(0,"\r");
    }

    writeBlock_ipbus(&hw,optarg,xx);
    printf("%d words loaded into ram %s at offset %x\n",(int)xx.size(),optarg,D0);


    break;



   case 'w':    //wait a bit
    usleep(atoi(optarg));
    break;

   case 'I':
   case 'J':
    {
      uint32_t slave_addr,wr,reg_addr,wr_data,mem,m16b;
//      ValWord< uint32_t > reg = hw.getNode ( "hgroc_i2c.ssettings_r" ).read();

      strtok(optarg,"=");
      p2=strtok(0,":");

      slave_addr=strtol(optarg,0,0);
      if (!p2) {
	wr=0;
	reg_addr=0;
	wr_data=0;
	mem=0;
      } else {
	wr=1;
	reg_addr=strtol(p2,0,0);
	p2=strtok(0,":");
	if (p2) {
	  wr_data=strtol(p2,0,0);
	  mem=1;
	} else {
	  wr_data=reg_addr;
	  reg_addr=0;
	  mem=0;
	}
	 m16b=0;
      }

//      printf("slave addr: 0x%x wr: 0x%x, reg_addr: 0x%x wr_data: 0x%x mem: 0x%x m16b: 0x%x\n",slave_addr,wr,reg_addr,wr_data,mem,m16b);

      uint32_t d;
      if (opt=='I') {
	main_i2c_init(&hw,0);
//wr=1 to write
//mem=0 to write single byte from wr_data
//mem=1 to write 2 bytes, first byte in reg_addr, second in wr_data
	d=main_i2c(&hw,slave_addr, wr, reg_addr, wr_data , mem , m16b   );
      } else  {
	hgroc_i2c_init(&hw,0);
	d=hgroc_i2c(&hw,slave_addr, wr, reg_addr, wr_data , mem , m16b   );
      }

      if ((d&0xc000000)!=0x4000000) printf("I2C error: 0x%x\n",d); else {
	if (wr==0) printf("%x\n",d&0xff);
      }
    }

  }
 }


 return 0;
}
