As a starting point and to enable some first tests of the digital paths from
the HGROC test boards, a firmware block that allows the data from the 320Mbit 
8-bit parallel data (stored in fifo0, see below), the trigger 0 and 1 streams 
(stored in fifo 1 and 2), and data32 (stored in fifo3).

All of these are stored MSB first in 32-bit fifos that can be dumped using e.g:
./fmc_hgroc  -R hgroc_daq.fifo0:2049
fifo0 is 2049 words, the others 1025, bit 0-3 of hgroc_daq.config is set if 
these are empty. Reading an empty fifo will return 0x1234567x

When changing the sampling delay the vtc* function of the i/o delays must be
disabled (bit 4 of hgroc_daq.config set to 0). This bit must be re-enabled
before taking data.
* Voltage and Temperature Compensation

The sampling delay of each bit of the 8-bit bus is set as the sum of the
subnodes "i1" "o1" and "i2" of e.g. hgroc_daq.delay.fifo0b0. Each register 
has a range of 9 bits. The total range should be at least 3.75ns, but 
interpreting these values as an actual delay requires some manipulation 
(please see the ultrascale selectio documentation).

The sampling delay of the 1280MHz inputs are set using e.g
hgroc_daq.delay.fifo1. This is also 9-bits and should have a range of at
least 1.25ns. In addition, it's possible to bit shift these data streams by
0-3 bits by writing e.g. hgroc_daq.bitshift.fifo1.


For loopback testing purposes the firmware also contains a test pattern
generator that streams the contents of hgroc_daq.o_ram out to trig1 and
trig2 (same data on both pairs of pins).


The capture of data and generation of various output signals is controlled
by a statemachine controlled by the hgroc_daq.event.* registers. The machine
started by writing to hgroc_daq.trig, and counts at 320MHz until
hgroc_daq.event.daq_stop is reached.


Please see sw/fmc_ctrl/test_daq1.sh for an example that can be used with a
loopback FMC that connects trig1 or 2 to data32.

