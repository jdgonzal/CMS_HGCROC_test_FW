Update from v0:

The 320Mbit data fifo is now split into 8 fifo0..7 for easier post-processing.
The trigger0, trigger1 and data32 fifos are called fifo8,9 and 10, and behave like 
fifo0-7 in most aspects except for the higher data rate.

The data from any fifo is dumped using e.g.:
./fmc_hgroc  -R hgroc_daq.fifo0:257
Each fifo currently holds up to 257 words of 32 bits each. Bit 0-10 of hgroc_daq.config 
is set if the corresponding fifo is empty. Reading an empty fifo will return 0x01234567.

When changing the sampling delay the vtc* function of the i/o delays must be
disabled (bit 4 of hgroc_daq.config set to 0). This bit must be re-enabled
before taking data.
* Voltage and Temperature Compensation

The sampling delay of each data line, and also the output delay of selected control 
lines is controlled by a register under hgroc_daq.delay.
Bits 11:10 bit selects the coarse delay in 0.7812ns increments, whereas 
the 9 least significant bits control the delay setting of the I/ODELAY3
element of the FPGA. The time step size of the xDELAY3 in the ultrascale
architecture is not specified, but is always more than 1/512. Thus, if the
register is treated as one number, the delay will jump at the 512-boundary 
transitions.

The capture of data and generation of various output signals is controlled
by a state-machine controlled by the hgroc_daq.event.* registers. The machine
started by writing to hgroc_daq.trig, and counts at 320MHz until
hgroc_daq.event.daq_stop is reached. Generating pulses shorter than one
320MHz cycle is not supported.

To start the state-machine write to hgroc_daq.trig. Write 0 if you want to
start capture at hgroc_daq.event.capture_start. Write 1 if you want to start
capture once the synchronisation pattern has been detected.

Please see sw/fmc_ctrl/test_daq1.sh for an example that can be used to
capture 320Mbit data.

